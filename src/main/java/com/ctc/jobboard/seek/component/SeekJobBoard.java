package com.ctc.jobboard.seek.component;

import java.util.ArrayList;
import java.util.Date;

import org.apache.commons.lang.StringUtils;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.core.JobBoardResponse;
import com.ctc.jobboard.core.JobContentJsonConverter;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.seek.domain.SeekBaseResponse;
import com.ctc.jobboard.seek.domain.SeekProcessingStatusRequest;
import com.ctc.jobboard.seek.domain.SeekProcessingStatusResponse;
import com.ctc.jobboard.seek.domain.alljobs.SeekGetAllAdvertismentsRequest;
import com.ctc.jobboard.seek.domain.apilinks.Links;
import com.ctc.jobboard.seek.domain.apilinks.SeekGetApiLinksRequest;
import com.ctc.jobboard.seek.domain.apilinks.SeekGetApiLinksResponse;
import com.ctc.jobboard.seek.domain.oauth.SeekOAuthRequest;
import com.ctc.jobboard.seek.domain.postjob.SeekJobExpiredRequest;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostRequest;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;
import com.ctc.jobboard.util.BasicConfig;

public abstract class SeekJobBoard extends JobBoard{
	
	public static final String DEFAULT_BOARD_NAME ;
	public static final String DEFAULT_BOARD_API_NAME ;
	
	
	public static final String ACCEPT_ALLJOBS = "application/vnd.seek.advertisement-list+json; version=1; charset=utf-8, application/vnd.seek.advertisement-error+json; version=1; charset=utf-8";
	public static final String ACCEPT = "application/vnd.seek.advertisement+json; version=1; charset=utf-8, application/vnd.seek.advertisement-error+json; version=1; charset=utf-8";
	public static final String CONTENT_TYPE_URLENCODED = "application/x-www-form-urlencoded";
	public static final String CONTENT_TYPE_JSON_PATCH = "application/vnd.seek.advertisement-patch+json; version=1; charset=utf-8";
	public static final String CONTENT_TYPE_JSON = "application/vnd.seek.advertisement+json; version=1; charset=utf-8";
	
	public static final String SERVICE_DOMAIN ;
	
	public static final String PROCESSING_STATUS_HEADER = "Processing-Status";
	
	static{
		
		SERVICE_DOMAIN = StringUtils.isEmpty( BasicConfig.get("seek_endpoint")) ? "https://adposting-integration.cloud.seek.com.au" : BasicConfig.get("seek_endpoint");
		DEFAULT_BOARD_NAME = StringUtils.isEmpty( BasicConfig.get("seek_default_jobboard_name")) ? "SEEK" : BasicConfig.get("seek_default_jobboard_name");
		DEFAULT_BOARD_API_NAME = StringUtils.isEmpty( BasicConfig.get("seek_default_jobboard_apiname")) ? "SEEK__c" : BasicConfig.get("seek_default_jobboard_apiname");
		
		requestEndpoints.put(SeekGetApiLinksRequest.class, SERVICE_DOMAIN + "/");
		requestEndpoints.put(SeekOAuthRequest.class, SERVICE_DOMAIN + "/auth/oauth2/token");
		requestEndpoints.put(SeekJobPostRequest.class, SERVICE_DOMAIN + "/advertisement");
		requestEndpoints.put(SeekJobExpiredRequest.class, SERVICE_DOMAIN + "/advertisement");
		requestEndpoints.put(SeekGetAllAdvertismentsRequest.class, SERVICE_DOMAIN + "/advertisement");
		
		
	}
	
	{
		successCodes.add(200);
		successCodes.add(201);
		successCodes.add(202);
		successCodes.add(422);
		successCodes.add(400);
		successCodes.add(403);
		
	}
	
	
	
	
	public SeekJobBoard( ) {
		
		this(DEFAULT_BOARD_NAME);
		
	}
	
	public SeekJobBoard(String jobBoardName) {
		
		super(jobBoardName, new JobContentJsonConverter());
		
	}

	public SeekJobBoard(String jobBoardName, String jobBoardSfApiName) {
		
		super(jobBoardName, jobBoardSfApiName, new JobContentJsonConverter());
		
	}
	
	public SeekAccount buildAccount(String advertiserId, String username, String password){
		return new SeekAccount(username, password, advertiserId);
	}
	

	public String getAccessToken(){
		String accessToken = "";
		SeekAccount account = (SeekAccount) getAccount();
		if(account != null){
			String currentToken = account.getAccessToken();
			accessToken = currentToken;
			int expireIn = account.getAccessTokenExpireIn();
			Date from = account.getAccessTokenFrom();
			
			
			if(accessToken==null 
					|| accessToken.equals("") 
					|| from == null 
					|| ((from.getTime() + expireIn * 1000) - new Date().getTime()   <= SeekOAuth.BEFORE_EXPIRED_TIME) ){
				
				SeekOAuth oauth = new SeekOAuth();
				accessToken  = oauth.retrieveAccessToken(account);
					
			}			
		}
		return accessToken;
	}
	
	public Links getApiLinks(){
		
		JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
		requestPackage.setMethodType(MethodType.GET);
		requestPackage.setRequest(new SeekGetApiLinksRequest());
		
		
		SeekGetApiLinksResponse response = execute(requestPackage,SeekGetApiLinksResponse.class);
		
		if(response != null){
			 return response.getLinks();
		}
		return null;
		
	}
	
	public String getAdvertisementLink(String advertisementId){
		Links links = getApiLinks();
		if(links != null && links.getAdvertisement()!=null && links.getAdvertisement().getHref()!=null){
			return SeekJobBoard.SERVICE_DOMAIN + links.getAdvertisement().getHref().replace("{advertisementId}", advertisementId);
		}
		return "";
	}
	
	public String getAdvertisementsLink(String advertiserId){
		Links links = getApiLinks();
		if(links != null && links.getAdvertisement()!=null && links.getAdvertisement().getHref()!=null){
			if(StringUtils.isEmpty(advertiserId)){
				return SeekJobBoard.SERVICE_DOMAIN + links.getAdvertisements().getHref().replace("{?advertiserId}", "");
			}else{
				return SeekJobBoard.SERVICE_DOMAIN + links.getAdvertisements().getHref().replace("{?advertiserId}", "?advertiserId="+advertiserId);
			}
			
		}
		return "";
	}
	
	public String getAdvertisementPath( ){
		Links links = getApiLinks();
		if(links != null && links.getAdvertisement()!=null && links.getAdvertisement().getHref()!=null){
			return SeekJobBoard.SERVICE_DOMAIN + links.getAdvertisement().getHref().replace("{advertisementId}", "");
		}
		return "";
	}
	
	public String getProcessingStatus(String advertisementLink) throws BadRequestException{
		JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
		requestPackage.setMethodType(MethodType.HEAD);
		requestPackage.setRequest(new SeekProcessingStatusRequest());
		requestPackage.setUrl(advertisementLink);
		
		
		SeekProcessingStatusResponse response = execute(requestPackage,SeekProcessingStatusResponse.class);
		
		return response.getHeader(PROCESSING_STATUS_HEADER);

	}
	
	public SeekJobPostResponse createJobPostResponse( String message, String code, String actionMessage, String referenceNo){
		if( referenceNo == null ){
			return null;
		}
		
		SeekJobPostResponse response = new SeekJobPostResponse();
		response.setMessage(message);
		response.setJobReference(referenceNo);
		response.setErrors(new ArrayList<com.ctc.jobboard.seek.domain.postjob.Error>());
		
		com.ctc.jobboard.seek.domain.postjob.Error error = new com.ctc.jobboard.seek.domain.postjob.Error();
		error.setCode(code);
		error.setField(actionMessage);
		
		response.getErrors().add(error);
		
		


		return response;
	}
	

	protected <R extends JobBoardResponse> R execute(JobBoardRequestPackage requestPackage, Class<R> clazz) {
		
		requestPackage.addHeader("Authorization", "Bearer "+ getAccessToken());
		
		return super.execute(requestPackage, clazz);
			
		
		
	}
	
	

	public SeekBaseResponse getResponse() {
		return (SeekBaseResponse) super.getResponse();
	}
	
	public void setResponse(SeekBaseResponse response) {
		super.setResponse(response);
		super.setResponsePackage(new SeekResponsePackage(response, actionType));
		
	}

	
	

	


}
