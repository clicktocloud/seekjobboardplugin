package com.ctc.jobboard.seek.component;

import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.seek.domain.SeekBaseRequest;
import com.ctc.jobboard.seek.domain.SeekBaseResponse;
import com.ctc.jobboard.seek.domain.postjob.SeekJobExpiredRequest;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;

public abstract class SeekJobPostBoards extends SeekJobBoard{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	protected String url;
	
	protected SeekBaseRequest request;
	
	
	public SeekJobPostBoards() {
		super();
	}
	
	public SeekJobPostBoards(String jobBoardName) {
		super(jobBoardName);
	}
	
	/**
	 * Constructor 
	 * 
	 * @param jobBoardName
	 * @param jobBoardSfApiName
	 */
	public SeekJobPostBoards(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
	}
	
	
	@Override
	public void post2JobBoard() {

		try {
		
			
			if( request != null){
				
				JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
				
				requestPackage.addHeader("Content-Type", SeekJobBoard.CONTENT_TYPE_JSON);
				
				if(JobBoard.INSERT.equals(this.actionType)){
					requestPackage.setMethodType(MethodType.POST);
				}else if(JobBoard.UPDATE.equals(this.actionType)){
					requestPackage.setMethodType(MethodType.PUT);
				}else if(JobBoard.ARCHIVE.equals(this.actionType)){
					requestPackage.setMethodType(MethodType.PATCH);
					requestPackage.setBody(SeekJobExpiredRequest.content);
					requestPackage.addHeader("Content-Type", SeekJobBoard.CONTENT_TYPE_JSON_PATCH);
				}

				requestPackage.addHeader("Accept", SeekJobBoard.ACCEPT);
				requestPackage.setRequest(request);
				
				requestPackage.setUrl(this.url);

				SeekBaseResponse response = execute(requestPackage, SeekJobPostResponse.class);
				
				setResponse( response );
				
			}else{
				logger.info("No any job to be posted from org ["+getCurrentSforg().getOrgId()+"]");
			}
		} catch (AccountException e) {
			
			throw new JobBoardException("AccountException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JobBoardException("Other error - " + e.getMessage());
		}
	}
	
	
	
	

	protected void throwJxtExceptionFromUnsuccessfulJobPostResponse(){
		if(getResponse() != null ){
//			if( getResponse().getResponseCode() != APIReturn.SUCCESS){
//				throw new JxtException("JXT Response - " + getResponse().getResponseCode()+" :" + getResponse().getResponseMessage());
//			}
			
		}	
	}
	
	

	

}
