package com.ctc.jobboard.seek.component;

import java.util.Date;

import com.ctc.jobboard.core.JobBoardRequestDispatcher;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.core.JobContentJsonConverter;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.seek.domain.oauth.SeekOAuthResponse;

public class SeekOAuth {
	public static final Integer BEFORE_EXPIRED_TIME = 1000; 
	public static final String TEST_CLIENT_ID = "0050569E4A781EE69BB16217DD7AE086";
	public static final String TEST_CLIENT_SECRET = "GAgfBwP8ywwFTwi6oGRRYTu5adsweKDC";
	public static final String TEST_ADVERTISER_ID = "34552313";
	private JobBoardRequestDispatcher service = new JobBoardRequestDispatcher();
	
	public String retrieveAccessToken(SeekAccount account){
		String token = "";
		
		JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
		requestPackage.setUrl(SeekJobBoard.SERVICE_DOMAIN + "/auth/oauth2/token");
		requestPackage.setMethodType(MethodType.POST);
		requestPackage.addHeader("Content-Type", SeekJobBoard.CONTENT_TYPE_URLENCODED);
		String bodyString = "client_id="+account.getUsername()+"&client_secret="+account.getPassword()+"&grant_type=client_credentials";
		requestPackage.setBody(bodyString);
		try {
			SeekOAuthResponse response = service.execute(requestPackage, SeekOAuthResponse.class, new JobContentJsonConverter());
			token = response.getAccessToken();
			
			account.setAccessToken(token);
			account.setAccessTokenExpireIn(response.getExpiresIn());
			account.setAccessTokenFrom( new Date());
			
		} catch (BadRequestException e) {
			
		}
		
		return token;
	}

}
