package com.ctc.jobboard.seek.component;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardResponse;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.seek.domain.SeekBaseResponse;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;

public class SeekResponsePackage implements JobBoardResponsePackage {
	
	private SeekBaseResponse response;
	
	private String action;
	
	private Summary summary;
	
	private Boolean hasErrors;
	private String errorMessage;
	private List<String> errorMessages;
	private List<Error> errors ;
	
	private List<Advertisement> created;
	private List<Advertisement> updated;
	private List<Advertisement> archived;
	
	private List<Advertisement> insertUpdated;
	

	public SeekResponsePackage(SeekBaseResponse response, String action) {
		super();
		this.response = response;
		this.action = action;
	}

	@Override
	final public Summary getSummary() {
		if( summary != null )
			return summary;
		summary = new Summary();
		summary.setSent(1);
		if(hasErrors() ){
			summary.setFailed(1);
		}else{
			if(response instanceof SeekJobPostResponse){
			
				summary.setInserted(getCreated().size());
				summary.setUpdated(getUpdated().size());
				summary.setArchived(getArchived().size());
			}
		}
		
		return summary;
		
	}

	@Override
	final public boolean hasErrors() {
		if(hasErrors != null)
			return hasErrors;
		
		if(response == null)
			hasErrors =  true;
		else
			hasErrors = response.getHttpStatusCode() != null  || !StringUtils.isEmpty(response.getMessage());
		return hasErrors;
	}
	
	final public String getErrorMessage(){
		if(errorMessage != null){
			return errorMessage;
		}
		
		if(getErrorMessages() != null && getErrorMessages().size() > 0 ){
			errorMessage = getErrorMessages().get(0);
		}
		
		return errorMessage;
	}
	
	final public List<String> getErrorMessages(){
		if(errorMessages != null){
			return errorMessages;
		}
		errorMessages = new ArrayList<String>();
		for(Error error : getErrors( )){
			errorMessages.add(error.getCode() + " - " + error.getMessage());
		}
		
		return errorMessages;
	}


	@Override
	final public List<Error> getErrors( ) {
		if(errors != null)
			return errors;
		
		errors =  new ArrayList<Error>();
		if( hasErrors() ){
			String message ="";
			if(response == null){
				 message = "Other exception !";
				 errors.add( new Error("Others",message));
			}
			 message= response.getMessage();
			
			if(StringUtils.isEmpty( message )){
				errors.add( new Error(response.getHttpStatusCode() +"" , response.getHttpMessage()));
				
			}else{
				if(response.getErrors() == null ||  response.getErrors().size() == 0){
					errors.add(new Error("Others",message));
				}else{
					for(com.ctc.jobboard.seek.domain.postjob.Error e : response.getErrors()){
						String field = e.getField() != null ? e.getField() : "";
						errors.add( new Error( e.getCode()+"/" +field ,e.getMessage()));
						
					}	
				}
			}
		}
		
		return errors;
	}


	@Override
	final public List<Error> getJobErrors() {
		
		return new ArrayList<Error>();
	}


	@Override
	final public List<Advertisement> getArchived() {
		if(archived != null)
			return archived;
		
		archived = new ArrayList<Advertisement>();
		if( ! hasErrors()  && this.response instanceof SeekJobPostResponse){
			SeekJobPostResponse response = (SeekJobPostResponse) this.response;
			
			if(isArchived()){
				Advertisement a = getAdvertisement(response);
				if(a != null)
					archived.add(a);
			}
		}
		
		return archived;
	}


	@Override
	final public List<Advertisement> getInsertUpdated() {
		if( insertUpdated != null ){
			return insertUpdated;
		}
		insertUpdated = new ArrayList<Advertisement>();
		if( ! hasErrors()  && this.response instanceof SeekJobPostResponse){
			SeekJobPostResponse response = (SeekJobPostResponse) this.response;
			if(! isArchived()){
				Advertisement a = getAdvertisement(response);
				if(a != null)
					insertUpdated.add(a);
			}	
		}
		
		return insertUpdated;
	}


	@Override
	final public List<Advertisement> getCreated() {
		if(created != null)
			return created;
		
		created = new ArrayList<Advertisement>();
		if( ! hasErrors()  && this.response instanceof SeekJobPostResponse){
			SeekJobPostResponse response = (SeekJobPostResponse) this.response;
			if(JobBoard.INSERT.equals(action) && ! isArchived()){
				Advertisement a = getAdvertisement(response);
				if(a != null)
					created.add(a);
			}
		}
		
		return created;
	}


	@Override
	final public List<Advertisement> getUpdated() {
		if(updated != null)
			return updated;
		
		updated = new ArrayList<Advertisement>();
		if( ! hasErrors()  && this.response instanceof SeekJobPostResponse){
			SeekJobPostResponse response = (SeekJobPostResponse) this.response;
			if(JobBoard.UPDATE.equals(action) && ! isArchived()){
				Advertisement a = getAdvertisement(response);
				if(a != null)
					updated.add(a);
			}
		}
		
		return updated;
	}
	
	private Advertisement getAdvertisement(SeekJobPostResponse response){
		if(response != null){
			String id = JobBoard.getAdId(response.getJobReference());
			String onlineAdUrl = SeekJobBoard.SERVICE_DOMAIN + response.getLinks().getSelf().getHref();
			String onlineOtherUrl = SeekJobBoard.SERVICE_DOMAIN + response.getLinks().getView().getHref();
			String onlineAdId = response.getId();
			if( ! StringUtils.isEmpty( id )){
				Advertisement a =  new Advertisement(id,onlineAdId,onlineAdUrl,action);
				a.setOnlineOtherUrl(onlineOtherUrl);
				
				return a;
			}	
		}
		
		return null;
		
	}
	
	private boolean isArchived(){
		return SeekJobPostResponse.State.EXPIRED.equals(((SeekJobPostResponse)response).getState());
	}

	@Override
	public JobBoardResponse getResponse() {
		return this.response;
	}

}
