package com.ctc.jobboard.seek.component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.defaultlist.SalaryRange;
import com.ctc.jobboard.seek.domain.ext.Classification;
import com.ctc.jobboard.seek.domain.ext.ItemType;
import com.ctc.jobboard.seek.domain.ext.Location;
import com.ctc.jobboard.util.BasicConfig;

public class SeekSettings {
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	private static boolean changed;
	
	public synchronized static boolean isChanged() {
		return changed;
	}
	
	public synchronized static void setChanged(boolean changed){
		SeekSettings.changed = changed;
	}


	private static String classificationsFileName = BasicConfig.get("seek_setting_classifications");
	private static List<Classification> classifications;
	private static List<Classification> subClassifications ;
	private static Long classificationLastTime = new Long(0);
	
	private static String areasFileName = BasicConfig.get("seek_setting_areas");
	private static Long areaLastTime = new Long(0);
	private static List<Location> areas ;
	
	private static String locationsFileName = BasicConfig.get("seek_setting_locations");
	private static List<Location> locations;
	private static Map<String, Location> locationsMap;
	private static List<Location> states ;
	private static List<Location> nations ;
	private static Long locationLastTime = new Long(0);
	
	private static String workTypesFileName = BasicConfig.get("seek_setting_worktypes");
	private static String salaryTypesFileName = BasicConfig.get("seek_setting_salarytypes");
	private static Long typeLastTime = new Long(0);
	private static List<ItemType> workTypes ;
	private static List<ItemType> salaryTypes ;
	
	private static String AUSalaryRangeFileName = BasicConfig.get("seek_setting_salaryranges_au");
	private static String UKSalaryRangeFileName = BasicConfig.get("seek_setting_salaryranges_uk");
	private static Long salaryRangeLastTime = new Long(0);
	private static SalaryRange auSalaryRange;
	private static SalaryRange ukSalaryRange ;
	
	
	public static List<Classification> getClassifications() {
		loadAll();
		return classifications;
	}
	
	public static List<Classification> getSubClassifications() {
		loadAll();
		
		return subClassifications;
	}


	public static List<Location> getNations() {
		loadAll();
		return nations;
	}
	
	public static List<Location> getStates() {
		loadAll();
		return states;
	}
	
	public static List<Location> getLocations() {
		loadAll();
		return locations;
	}


	public static List<ItemType> getWorkTypes() {
		loadAll();
		return workTypes;
	}


	public static List<ItemType> getSalaryTypes() {
		loadAll();
		return salaryTypes;
	}


	public static SalaryRange getAuSalaryRange() {
		loadAll();
		return auSalaryRange;
	}


	public static SalaryRange getUkSalaryRange() {
		loadAll();
		return ukSalaryRange;
	}


	private static List<String> read(File file) throws FileNotFoundException, IOException{
		
		changed = true;
		
		List<String> lines = IOUtils.readLines(new FileReader(file));
		
		if( lines != null && lines.size() > 1){
			return lines.subList(1, lines.size());
		}
		
		return null;
	}
	

	private static URI getFileUri(String filename){
		URI uri = null;
		try {
			
			String filefullpath = BasicConfig.get("seek_setting_file_path");
			
			if(StringUtils.isEmpty(filefullpath)){
				
				uri= SeekSettings.class.getResource(filename).toURI();
				
			}else{
				filefullpath = "file:"+filefullpath + filename;
				
				uri = new URI(filefullpath);
			}
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		return uri;
	}
	
	public static void loadAll(){
		loadClassifications();
		loadLocations();
		loadAreas();
		loadWorkTypes();
		loadSalaryTypes();
		loadAuSalaryRange();
		loadUkSalaryRange();
	}
	
	public static List<Classification> loadClassifications(){
		
		List<String> lines = null;
		URI uri = getFileUri(classificationsFileName);

		if(uri == null){
			return null;
		}
	
		File file = new File(uri);
		if(! file.exists()){
			return null;
		}
		
		Map<String, Classification> classificationMap = new HashMap<String, Classification>();
		

		synchronized (classificationLastTime) {
			long last = file.lastModified();
			if(last == classificationLastTime && classifications!= null){
				return classifications;
			}
			
			
			classificationLastTime = last;
			
			subClassifications = new LinkedList<Classification>();
			
			try {
				lines = read(file);

				if (lines == null)
					return null;

				for (String line : lines) {
					if (!StringUtils.isEmpty(line)) {
						String[] columns = line.split(BasicConfig.CSV_SPLITTER);
						fixColumns(columns);
						Classification cf = classificationMap.get(columns[5]);
						if(cf == null){
							cf = new Classification(columns[5], columns[6],
									columns[4],"");
							classificationMap.put(cf.getId(), cf);
						}
								
						
						Classification sub = new Classification(
								columns[1], columns[2], columns[0],columns[5]);
						
						cf.addSubClassification(sub);
						
						subClassifications.add(sub);
						

					}
				}

				classifications = new LinkedList<Classification>(classificationMap.values());

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		
		//Collections.sort(subClassifications,ITEM_COMPARATOR);
		//Collections.sort(classifications,ITEM_COMPARATOR);
		
		return classifications;
	}
	
	public static List<Location> loadLocations(){
		
		List<String> lines = null;
		URI uri = getFileUri(locationsFileName);

		if(uri == null){
			return null;
		}
	
		File file = new File(uri);
		if(! file.exists()){
			return null;
		}
		
		Map<String,Location> nationMap = new HashMap<String,Location>();
		Map<String,Location> stateMap = new HashMap<String,Location>();
		

		synchronized (locationLastTime) {
			long last = file.lastModified();
			if(last == locationLastTime && nations!=null){
				return nations;
			}
			
			locationLastTime = last;
			
			try {
				lines = read(file);

				if (lines == null)
					return null;

				
				locationsMap = new HashMap<String, Location>();
				
				for (String line : lines) {
					if (!StringUtils.isEmpty(line)) {
						String[] columns = line.split(BasicConfig.CSV_SPLITTER);
						
						fixColumns(columns);
						Location nation =nationMap.get(columns[7]);
						if(nation == null){
							nation = new Location(columns[7],columns[8],columns[6],"");
							nationMap.put(nation.getId(), nation);
						}
						
						Location state = stateMap.get(columns[4]);
						if(state == null){
							state = new Location(columns[4],columns[5],columns[3],columns[7]);
							stateMap.put(state.getId(), state);
							nation.addChild(state);
						}
						
						
						
						Location location = new Location(columns[1],columns[2],columns[0],columns[4]);
						
						locationsMap.put(location.getId(), location);
						state.addChild(location);

					}
				}
				
				locations = new LinkedList<Location>(locationsMap.values());
				
				states = new LinkedList<Location>(stateMap.values());

				nations = new LinkedList<Location>(nationMap.values());

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		//Collections.sort(locations,ITEM_COMPARATOR);
		//Collections.sort(states,ITEM_COMPARATOR);
		//Collections.sort(nations,ITEM_COMPARATOR);
		return nations;
	}
	
	public static List<Location> loadAreas(){
		
		if(locationsMap == null || locationsMap.size() == 0 ){
			logger.error("No any location has been loaded !");
			return null;
		}
		
		List<String> lines = null;
		URI uri = getFileUri(areasFileName);

		if(uri == null){
			return null;
		}
	
		File file = new File(uri);
		if(! file.exists()){
			return null;
		}
		
		synchronized (areaLastTime) {
			long last = file.lastModified();
			if(last == areaLastTime && areas!=null){
				return areas;
			}
			
			areaLastTime = last;
			
			try {
				lines = read(file);

				if (lines == null)
					return null;

				areas = new LinkedList<Location>();
				
				for (String line : lines) {
					if (!StringUtils.isEmpty(line)) {
						String[] columns = line.split(BasicConfig.CSV_SPLITTER);
						fixColumns(columns);
						Location area = new Location(columns[1],columns[2],columns[0],columns[4]);
						areas.add(area);
						
						Location location = locationsMap.get(columns[4]);
						if(location != null){
							location.addChild(area);
						}
					}
				}
				

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		
		
		return areas;
	}
	
	public static List<ItemType> loadWorkTypes(){
		 workTypes  = loadItemTypes(workTypesFileName,workTypes);
		 return workTypes;
	}
	
	public static List<ItemType> loadSalaryTypes(){
		salaryTypes =  loadItemTypes(salaryTypesFileName,salaryTypes);
		return salaryTypes;
	}
	
	private static List<ItemType> loadItemTypes(String filename, List<ItemType> exists){
		
		
		
		List<String> lines = null;
		URI uri = getFileUri(filename);

		if(uri == null){
			return null;
		}
	
		File file = new File(uri);
		if(! file.exists()){
			return null;
		}
		
		List<ItemType> types = null;
		
		synchronized (typeLastTime) {
			long last = file.lastModified();
			if(last == typeLastTime && exists!=null){
				return exists;
			}
			
			typeLastTime = last;
			
			
			
			try {
				lines = read(file);

				if (lines == null)
					return null;

				 types = new LinkedList<ItemType>();
				
				for (String line : lines) {
					if (!StringUtils.isEmpty(line)) {
						String[] columns = line.split(BasicConfig.CSV_SPLITTER);
						fixColumns(columns);
						ItemType type = new ItemType(columns[1],columns[0]);
						types.add(type);
	
					}
				}
				

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		
		return types;
	}
	
	
	public  static SalaryRange loadAuSalaryRange(){
		auSalaryRange = loadSalaryRange(AUSalaryRangeFileName, auSalaryRange,"AU_NZ");
		
		return auSalaryRange;
	}
	
	public  static SalaryRange loadUkSalaryRange(){
		ukSalaryRange = loadSalaryRange(UKSalaryRangeFileName, ukSalaryRange,"UK");
		
		return ukSalaryRange;
	}
		
	
	private static SalaryRange loadSalaryRange(String filename, SalaryRange exist, String name){
		
		
		
		List<String> lines = null;
		URI uri = getFileUri(filename);

		if(uri == null){
			return null;
		}
	
		File file = new File(uri);
		if(! file.exists()){
			return null;
		}
		
		SalaryRange range = null;
		
		synchronized (salaryRangeLastTime) {
			long last = file.lastModified();
			if(last == salaryRangeLastTime && exist!=null){
				return exist;
			}
			
			salaryRangeLastTime = last;
			
			
			
			try {
				lines = read(file);

				if (lines == null)
					return null;

				range = new SalaryRange(name);
				
				Map<String, SalaryRange.Band> bandsMap = new HashMap<String, SalaryRange.Band>();
				
				String previousBand = "";
				
				for (String line : lines) {
					if (!StringUtils.isEmpty(line)) {
						String[] columns = line.split(BasicConfig.CSV_SPLITTER);
						fixColumns(columns);
						String currentBand = "";
						if(columns.length >= 5){
							currentBand = columns[4];
						}
						if(StringUtils.isEmpty( currentBand)){
							currentBand = previousBand;
						}else{
							previousBand = currentBand;
						}
						SalaryRange.Band band  = bandsMap.get(currentBand);
						if( band == null){
							band = new SalaryRange.Band(currentBand);
							bandsMap.put(band.getName(), band);
							range.addBand(band);
						}
						
						if(StringUtils.isEmpty(columns[0]) && StringUtils.isEmpty(columns[1])){
							
						}else{
							band.addAnnualRange(new SalaryRange.Range(columns[0], columns[1]));
						}
						
						String hourlyFrom ="";
						String hourlyTo = "";
						if(columns.length >= 3){
							hourlyFrom = columns[2];
						}
						
						if(columns.length >= 4){
							hourlyTo = columns[3];
						}
						
						if(StringUtils.isEmpty(hourlyFrom) && StringUtils.isEmpty(hourlyTo)){
							
						}else{
							band.addHourlyRange(new SalaryRange.Range(hourlyFrom, hourlyTo));
						}
						
						
		
					}
				}
				

			} catch (Exception e) {

				e.printStackTrace();
			}
		}
		return range;
	}
	
	
	private static void fixColumns(String[] columns){
		if(columns != null){
			for(int i = 0;i < columns.length ; i ++ ){
				if(columns[i] != null && columns[i] .startsWith("\"") && columns[i] .endsWith("\"")){
					columns[i]  = columns[i] .replace("\"", "");
				}
			}
		}
	}
	
	public static final Comparator<ItemType>  ITEM_COMPARATOR = new Comparator<ItemType>(){
		 @Override
		    public int compare(ItemType left, ItemType right) {
			 	if(left == null && right == null){
			 		return 0;
			 	}else if( left == null){
			 		return -1;
			 	}
			 	
		        return left.getDescription().compareTo(right.getDescription()); 
		    }
	};
	
	
}
