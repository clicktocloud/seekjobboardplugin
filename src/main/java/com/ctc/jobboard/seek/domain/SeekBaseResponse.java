package com.ctc.jobboard.seek.domain;

import java.util.List;

import com.ctc.jobboard.core.JobBoardResponse;
import com.ctc.jobboard.seek.domain.postjob.Error;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "message",
    "errors"
})
public class SeekBaseResponse extends JobBoardResponse{
	
	@JsonProperty("message")
    private String message;
    @JsonProperty("errors")
    private List<Error> errors = null;
    
    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }
    


    @JsonProperty("errors")
    public List<Error> getErrors() {
        return errors;
    }

    @JsonProperty("errors")
    public void setErrors(List<Error> errors) {
        this.errors = errors;
    }
	
	
}
