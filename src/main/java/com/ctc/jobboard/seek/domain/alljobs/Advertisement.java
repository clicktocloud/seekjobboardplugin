
package com.ctc.jobboard.seek.domain.alljobs;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "advertiserId",
    "jobTitle",
    "jobReference",
    "_links"
})
public class Advertisement {

    @JsonProperty("id")
    private String id;
    @JsonProperty("advertiserId")
    private String advertiserId;
    @JsonProperty("jobTitle")
    private String jobTitle;
    @JsonProperty("jobReference")
    private String jobReference;
    @JsonProperty("_links")
    private Links links;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("advertiserId")
    public String getAdvertiserId() {
        return advertiserId;
    }

    @JsonProperty("advertiserId")
    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    @JsonProperty("jobTitle")
    public String getJobTitle() {
        return jobTitle;
    }

    @JsonProperty("jobTitle")
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    @JsonProperty("jobReference")
    public String getJobReference() {
        return jobReference;
    }

    @JsonProperty("jobReference")
    public void setJobReference(String jobReference) {
        this.jobReference = jobReference;
    }

    @JsonProperty("_links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Links links) {
        this.links = links;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
