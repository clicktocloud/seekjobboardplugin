
package com.ctc.jobboard.seek.domain.alljobs;

import java.util.ArrayList;
import java.util.List;

import com.ctc.jobboard.seek.domain.SeekBaseResponse;

public class SeekAdvertismentsList extends SeekBaseResponse {

   private List<Advertisement> advertisements = new ArrayList<Advertisement>();
   
   public SeekAdvertismentsList(List<Advertisement> advertisements) {
		super();
		this.advertisements = advertisements;
	}

	public List<Advertisement> getAdvertisements() {
		return advertisements;
	}
	
	public void setAdvertisements(List<Advertisement> advertisements) {
		this.advertisements = advertisements;
	}

	

}
