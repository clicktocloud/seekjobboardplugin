
package com.ctc.jobboard.seek.domain.apilinks;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "href",
    "templated"
})
public class Advertisement {

    /**
     * The link template for advertisement *get processing status*, *update* and *expire* operations.
     * (Required)
     * 
     */
    @JsonProperty("href")
    @JsonPropertyDescription("The link template for advertisement *get processing status*, *update* and *expire* operations.")
    private String href;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("templated")
    private Boolean templated;

    /**
     * The link template for advertisement *get processing status*, *update* and *expire* operations.
     * (Required)
     * 
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * The link template for advertisement *get processing status*, *update* and *expire* operations.
     * (Required)
     * 
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("templated")
    public Boolean getTemplated() {
        return templated;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("templated")
    public void setTemplated(Boolean templated) {
        this.templated = templated;
    }

}
