
package com.ctc.jobboard.seek.domain.apilinks;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "advertisements",
    "advertisement"
})
public class Links {

    @JsonProperty("advertisements")
    private Advertisements advertisements;
    @JsonProperty("advertisement")
    private Advertisement advertisement;

    @JsonProperty("advertisements")
    public Advertisements getAdvertisements() {
        return advertisements;
    }

    @JsonProperty("advertisements")
    public void setAdvertisements(Advertisements advertisements) {
        this.advertisements = advertisements;
    }

    @JsonProperty("advertisement")
    public Advertisement getAdvertisement() {
        return advertisement;
    }

    @JsonProperty("advertisement")
    public void setAdvertisement(Advertisement advertisement) {
        this.advertisement = advertisement;
    }

}
