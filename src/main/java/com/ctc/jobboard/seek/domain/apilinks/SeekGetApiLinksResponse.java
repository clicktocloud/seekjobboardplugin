
package com.ctc.jobboard.seek.domain.apilinks;

import com.ctc.jobboard.seek.domain.SeekBaseResponse;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "_links"
})
public class SeekGetApiLinksResponse extends SeekBaseResponse {

    @JsonProperty("_links")
    private Links links;

    @JsonProperty("_links")
    public Links getLinks() {
        return links;
    }

    @JsonProperty("_links")
    public void setLinks(Links links) {
        this.links = links;
    }

}
