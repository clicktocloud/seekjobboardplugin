package com.ctc.jobboard.seek.domain.ext;

import java.util.ArrayList;
import java.util.List;

public class Classification extends ItemType{
	
	
	
	private String type;
	
	
	
	
	public Classification(String id, String type, String description,String parentId) {
		super(id,description);
		
		this.type = type;
		
		this.parentId = parentId;
	}

	private List<Classification> subClassifications;
	
	private String parentId;

	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	

	
	
	public void addSubClassification(Classification sub){
		if(subClassifications == null){
			subClassifications = new ArrayList<Classification>();
		}
		subClassifications.add(sub);
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public void setSubClassifications(List<Classification> subClassifications) {
		this.subClassifications = subClassifications;
	}

	public List<Classification> getSubClassifications() {
		return subClassifications;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Classification other = (Classification) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
