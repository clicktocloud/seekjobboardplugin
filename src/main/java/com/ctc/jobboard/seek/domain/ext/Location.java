package com.ctc.jobboard.seek.domain.ext;

import java.util.ArrayList;
import java.util.List;

public class Location extends ItemType{
	
	private String type;
	
	
	
	public Location(String id, String type, String description, String parentId) {
		super(id,description);
		
		this.type = type;
		
		this.parentId = parentId;
	}

	private List<Location> children;
	
	private String  parentId;

	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	
	
	public void addChild(Location child){
		if(children == null){
			children = new ArrayList<Location>();
		}
		children.add(child);
	}

	public List<Location> getChildren() {
		return children;
	}

	public void setChildren(List<Location> children) {
		this.children = children;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	
}
