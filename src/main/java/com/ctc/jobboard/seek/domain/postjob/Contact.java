
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * name, phone, email
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "phone",
    "email"
})
public class Contact {

    /**
     * Name of contact person. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("name")
    @JsonPropertyDescription("Name of contact person. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String name;
    /**
     * Contact phone number. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("phone")
    @JsonPropertyDescription("Contact phone number. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String phone;
    /**
     * Contact email address.
     * 
     */
    @JsonProperty("email")
    @JsonPropertyDescription("Contact email address.")
    private String email;

    /**
     * Name of contact person. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * Name of contact person. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Contact phone number. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("phone")
    public String getPhone() {
        return phone;
    }

    /**
     * Contact phone number. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("phone")
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Contact email address.
     * 
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * Contact email address.
     * 
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

}
