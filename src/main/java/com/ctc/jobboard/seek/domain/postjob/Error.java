
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "field",
    "code"
})
public class Error {

    @JsonProperty("field")
    private String field;
    @JsonProperty("code")
    private String code;
    
    @JsonProperty("message")
    private String message;

    @JsonProperty("field")
    public String getField() {
        return field;
    }

    @JsonProperty("field")
    public void setField(String field) {
        this.field = field;
    }

    @JsonProperty("code")
    public String getCode() {
        return code;
    }

    @JsonProperty("code")
    public void setCode(String code) {
        this.code = code;
    }

    @JsonProperty("message")
	public String getMessage() {
		return message;
	}

    @JsonProperty("message")
	public void setMessage(String message) {
		this.message = message;
	}

}
