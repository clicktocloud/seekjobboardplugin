
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "self",
    "view"
})
public class Links {

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("self")
    private Self self;
    @JsonProperty("view")
    private View view;

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("self")
    public Self getSelf() {
        return self;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("self")
    public void setSelf(Self self) {
        this.self = self;
    }

    @JsonProperty("view")
    public View getView() {
        return view;
    }

    @JsonProperty("view")
    public void setView(View view) {
        this.view = view;
    }

}
