
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * The location and area of the job. When area is provided, the area must be within the location.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "areaId"
})
public class Location {

    /**
     * The location id.
     * (Required)
     * 
     */
    @JsonProperty("id")
    @JsonPropertyDescription("The location id.")
    private String id;
    /**
     * The area id which must be within the location id.
     * 
     */
    @JsonProperty("areaId")
    @JsonPropertyDescription("The area id which must be within the location id.")
    private String areaId;

    /**
     * The location id.
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * The location id.
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * The area id which must be within the location id.
     * 
     */
    @JsonProperty("areaId")
    public String getAreaId() {
        return areaId;
    }

    /**
     * The area id which must be within the location id.
     * 
     */
    @JsonProperty("areaId")
    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

}
