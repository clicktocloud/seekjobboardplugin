
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "fullName",
    "email",
    "teamName"
})
public class Recruiter {

    /**
     * The first name and surname separated by a space of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.
     * (Required)
     * 
     */
    @JsonProperty("fullName")
    @JsonPropertyDescription("The first name and surname separated by a space of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.")
    private String fullName;
    /**
     * The email address of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.
     * (Required)
     * 
     */
    @JsonProperty("email")
    @JsonPropertyDescription("The email address of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.")
    private String email;
    /**
     * The team name of recruiter who is responsible for the job ad and handling the recruitment of the position is part of. This information will not be visible on the job ad.
     * 
     */
    @JsonProperty("teamName")
    @JsonPropertyDescription("The team name of recruiter who is responsible for the job ad and handling the recruitment of the position is part of. This information will not be visible on the job ad.")
    private String teamName;

    /**
     * The first name and surname separated by a space of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.
     * (Required)
     * 
     */
    @JsonProperty("fullName")
    public String getFullName() {
        return fullName;
    }

    /**
     * The first name and surname separated by a space of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.
     * (Required)
     * 
     */
    @JsonProperty("fullName")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * The email address of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.
     * (Required)
     * 
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * The email address of recruiter who is responsible for the job ad and handling the recruitment of the position. This information will not be visible on the job ad.
     * (Required)
     * 
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * The team name of recruiter who is responsible for the job ad and handling the recruitment of the position is part of. This information will not be visible on the job ad.
     * 
     */
    @JsonProperty("teamName")
    public String getTeamName() {
        return teamName;
    }

    /**
     * The team name of recruiter who is responsible for the job ad and handling the recruitment of the position is part of. This information will not be visible on the job ad.
     * 
     */
    @JsonProperty("teamName")
    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

}
