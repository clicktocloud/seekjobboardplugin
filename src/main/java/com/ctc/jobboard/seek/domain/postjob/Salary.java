
package com.ctc.jobboard.seek.domain.postjob;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * Information about the salary for the job.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "type",
    "minimum",
    "maximum",
    "details"
})
public class Salary {

    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("type")
    @JsonPropertyDescription("Value must be one of:")
    private Salary.Type type;
    /**
     * Minimum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.
     * (Required)
     * 
     */
    @JsonProperty("minimum")
    @JsonPropertyDescription("Minimum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.")
    private BigDecimal minimum;
    /**
     * Maximum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.
     * (Required)
     * 
     */
    @JsonProperty("maximum")
    @JsonPropertyDescription("Maximum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.")
    private BigDecimal maximum;
    /**
     * Optional string used to specify salary information for display to candidates [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("details")
    @JsonPropertyDescription("Optional string used to specify salary information for display to candidates [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String details;

    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("type")
    public Salary.Type getType() {
        return type;
    }

    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("type")
    public void setType(Salary.Type type) {
        this.type = type;
    }

    /**
     * Minimum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.
     * (Required)
     * 
     */
    @JsonProperty("minimum")
    public BigDecimal getMinimum() {
        return minimum;
    }

    /**
     * Minimum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.
     * (Required)
     * 
     */
    @JsonProperty("minimum")
    public void setMinimum(BigDecimal minimum) {
        this.minimum = minimum;
    }

    /**
     * Maximum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.
     * (Required)
     * 
     */
    @JsonProperty("maximum")
    public BigDecimal getMaximum() {
        return maximum;
    }

    /**
     * Maximum salary of the salary range applicable to the job advertisement. This is not displayed to candidates.
     * (Required)
     * 
     */
    @JsonProperty("maximum")
    public void setMaximum(BigDecimal maximum) {
        this.maximum = maximum;
    }

    /**
     * Optional string used to specify salary information for display to candidates [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("details")
    public String getDetails() {
        return details;
    }

    /**
     * Optional string used to specify salary information for display to candidates [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("details")
    public void setDetails(String details) {
        this.details = details;
    }

    public enum Type {

        ANNUAL_PACKAGE("AnnualPackage"),
        ANNUAL_COMMISSION("AnnualCommission"),
        COMMISSION_ONLY("CommissionOnly"),
        HOURLY_RATE("HourlyRate");
        private final String value;
        private final static Map<String, Salary.Type> CONSTANTS = new HashMap<String, Salary.Type>();

        static {
            for (Salary.Type c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Type(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static Salary.Type fromValue(String value) {
            Salary.Type constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
