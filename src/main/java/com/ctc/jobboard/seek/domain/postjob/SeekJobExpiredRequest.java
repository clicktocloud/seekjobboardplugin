
package com.ctc.jobboard.seek.domain.postjob;

import com.ctc.jobboard.seek.domain.SeekBaseRequest;

public class SeekJobExpiredRequest extends SeekBaseRequest {
	
	public static String content = "[" +
	                                "{" +
	                                    "\"path\": \"state\"," +
	                                    "\"op\": \"replace\"," +
	                                    "\"value\": \"Expired\""+
	                                  "}" +
	                                "]";
	

}
