
package com.ctc.jobboard.seek.domain.postjob;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ctc.jobboard.seek.domain.SeekBaseResponse;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
   
    "thirdParties",
    "advertisementType",
    "jobTitle",
    "searchJobTitle",
    "location",
    "subclassificationId",
    "workType",
    "salary",
    "jobSummary",
    "advertisementDetails",
    "contact",
    "video",
    "applicationEmail",
    "applicationFormUrl",
    "endApplicationUrl",
    "screenId",
    "jobReference",
    "agentJobReference",
    "template",
    "standout",
    "recruiter",
    "additionalProperties",
    "id",
    "expiryDate",
    "state",
    "_links"
})
public class SeekJobPostResponse extends SeekBaseResponse{
	
	
	  
	
	
	

   

    /**
     * Required when creating or updating an advertisement on behalf of an advertiser. Must not be supplied when advertiser creates or updates the advertisement as themselves. When this attribute is supplied then advertiserId is required. When this attribute is supplied then agentId should only be supplied when the caller is posting an advertisement on behalf of the agent i.e. the caller is not the agent.
     * 
     */
    @JsonProperty("thirdParties")
    @JsonPropertyDescription("Required when creating or updating an advertisement on behalf of an advertiser. Must not be supplied when advertiser creates or updates the advertisement as themselves. When this attribute is supplied then advertiserId is required. When this attribute is supplied then agentId should only be supplied when the caller is posting an advertisement on behalf of the agent i.e. the caller is not the agent.")
    private ThirdParties thirdParties;
    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("advertisementType")
    @JsonPropertyDescription("Value must be one of:")
    private SeekJobPostResponse.AdvertisementType advertisementType;
    /**
     * Defines the title of the job role or occupation which is shown to job seekers [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("jobTitle")
    @JsonPropertyDescription("Defines the title of the job role or occupation which is shown to job seekers [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String jobTitle;
    /**
     * Defines the search title of the job role or occupation which is used by the SEEK search engine [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc. When this field is not provided, the *jobTitle* is used as the search title.
     * 
     */
    @JsonProperty("searchJobTitle")
    @JsonPropertyDescription("Defines the search title of the job role or occupation which is used by the SEEK search engine [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc. When this field is not provided, the *jobTitle* is used as the search title.")
    private String searchJobTitle;
    /**
     * The location and area of the job. When area is provided, the area must be within the location.
     * (Required)
     * 
     */
    @JsonProperty("location")
    @JsonPropertyDescription("The location and area of the job. When area is provided, the area must be within the location.")
    private Location location;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("subclassificationId")
    private String subclassificationId;
    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("workType")
    @JsonPropertyDescription("Value must be one of:")
    private SeekJobPostResponse.WorkType workType;
    /**
     * Information about the salary for the job.
     * (Required)
     * 
     */
    @JsonProperty("salary")
    @JsonPropertyDescription("Information about the salary for the job.")
    private Salary salary;
    /**
     * Description that is present in search results [limited to 150 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("jobSummary")
    @JsonPropertyDescription("Description that is present in search results [limited to 150 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String jobSummary;
    /**
     * Full details of the job [limited to 20000 characters]. Basic formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("advertisementDetails")
    @JsonPropertyDescription("Full details of the job [limited to 20000 characters]. Basic formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String advertisementDetails;
    /**
     * name, phone, email
     * 
     */
    @JsonProperty("contact")
    @JsonPropertyDescription("name, phone, email")
    private Contact contact;
    /**
     * An optional video related to the job and its postition within the advertisement. Provided link must be secure (HTTPS) to be accepted
     * 
     */
    @JsonProperty("video")
    @JsonPropertyDescription("An optional video related to the job and its postition within the advertisement. Provided link must be secure (HTTPS) to be accepted")
    private Video video;
    /**
     * Email applications directed to.
     * 
     */
    @JsonProperty("applicationEmail")
    @JsonPropertyDescription("Email applications directed to.")
    private String applicationEmail;
    /**
     * The URL of the Job Application Form if not on SEEK [limited to 500 characters].
     * 
     */
    @JsonProperty("applicationFormUrl")
    @JsonPropertyDescription("The URL of the Job Application Form if not on SEEK [limited to 500 characters].")
    private String applicationFormUrl;
    /**
     * The URL that the candidate lands on at the end of the application [limited to 500 characters].
     * 
     */
    @JsonProperty("endApplicationUrl")
    @JsonPropertyDescription("The URL that the candidate lands on at the end of the application [limited to 500 characters].")
    private String endApplicationUrl;
    /**
     * The ID number of an existing SEEK Screen to attach to the job advertisement
     * 
     */
    @JsonProperty("screenId")
    @JsonPropertyDescription("The ID number of an existing SEEK Screen to attach to the job advertisement")
    private Integer screenId;
    /**
     * A quotable reference code used by the advertiser to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("jobReference")
    @JsonPropertyDescription("A quotable reference code used by the advertiser to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String jobReference;
    /**
     * An additional reference code used by the agent to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("agentJobReference")
    @JsonPropertyDescription("An additional reference code used by the agent to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private String agentJobReference;
    /**
     * When a custom template is to be used for the advertisement, it's ID and custom field values can be specified here.
     * 
     */
    @JsonProperty("template")
    @JsonPropertyDescription("When a custom template is to be used for the advertisement, it's ID and custom field values can be specified here.")
    private Template template;
    /**
     * When the *advertisementType* is *StandOut* then this attribute contains standout advertisement values.
     * 
     */
    @JsonProperty("standout")
    @JsonPropertyDescription("When the *advertisementType* is *StandOut* then this attribute contains standout advertisement values.")
    private Standout standout;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("recruiter")
    private Recruiter recruiter;
    /**
     * When *ResidentsOnly* is present, the job will specify that it is available for Australian/NZ residents only.
     * 
     */
    @JsonProperty("additionalProperties")
    @JsonPropertyDescription("When *ResidentsOnly* is present, the job will specify that it is available for Australian/NZ residents only.")
    private List<Object> additionalProperties = null;
    /**
     * the ID of the advertisement. Can be used with link templates from the API links endpoint.
     * (Required)
     * 
     */
    @JsonProperty("id")
    @JsonPropertyDescription("the ID of the advertisement. Can be used with link templates from the API links endpoint.")
    private String id;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("expiryDate")
    private String expiryDate;
    /**
     * Value is one of:
     * (Required)
     * 
     */
    @JsonProperty("state")
    @JsonPropertyDescription("Value is one of:")
    private SeekJobPostResponse.State state;
    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("_links")
    private Links links;

    /**
     * Required when creating or updating an advertisement on behalf of an advertiser. Must not be supplied when advertiser creates or updates the advertisement as themselves. When this attribute is supplied then advertiserId is required. When this attribute is supplied then agentId should only be supplied when the caller is posting an advertisement on behalf of the agent i.e. the caller is not the agent.
     * 
     */
    @JsonProperty("thirdParties")
    public ThirdParties getThirdParties() {
        return thirdParties;
    }

    /**
     * Required when creating or updating an advertisement on behalf of an advertiser. Must not be supplied when advertiser creates or updates the advertisement as themselves. When this attribute is supplied then advertiserId is required. When this attribute is supplied then agentId should only be supplied when the caller is posting an advertisement on behalf of the agent i.e. the caller is not the agent.
     * 
     */
    @JsonProperty("thirdParties")
    public void setThirdParties(ThirdParties thirdParties) {
        this.thirdParties = thirdParties;
    }

    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("advertisementType")
    public SeekJobPostResponse.AdvertisementType getAdvertisementType() {
        return advertisementType;
    }

    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("advertisementType")
    public void setAdvertisementType(SeekJobPostResponse.AdvertisementType advertisementType) {
        this.advertisementType = advertisementType;
    }

    /**
     * Defines the title of the job role or occupation which is shown to job seekers [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("jobTitle")
    public String getJobTitle() {
        return jobTitle;
    }

    /**
     * Defines the title of the job role or occupation which is shown to job seekers [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("jobTitle")
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }

    /**
     * Defines the search title of the job role or occupation which is used by the SEEK search engine [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc. When this field is not provided, the *jobTitle* is used as the search title.
     * 
     */
    @JsonProperty("searchJobTitle")
    public String getSearchJobTitle() {
        return searchJobTitle;
    }

    /**
     * Defines the search title of the job role or occupation which is used by the SEEK search engine [limited to 80 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc. When this field is not provided, the *jobTitle* is used as the search title.
     * 
     */
    @JsonProperty("searchJobTitle")
    public void setSearchJobTitle(String searchJobTitle) {
        this.searchJobTitle = searchJobTitle;
    }

    /**
     * The location and area of the job. When area is provided, the area must be within the location.
     * (Required)
     * 
     */
    @JsonProperty("location")
    public Location getLocation() {
        return location;
    }

    /**
     * The location and area of the job. When area is provided, the area must be within the location.
     * (Required)
     * 
     */
    @JsonProperty("location")
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("subclassificationId")
    public String getSubclassificationId() {
        return subclassificationId;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("subclassificationId")
    public void setSubclassificationId(String subclassificationId) {
        this.subclassificationId = subclassificationId;
    }

    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("workType")
    public SeekJobPostResponse.WorkType getWorkType() {
        return workType;
    }

    /**
     * Value must be one of:
     * (Required)
     * 
     */
    @JsonProperty("workType")
    public void setWorkType(SeekJobPostResponse.WorkType workType) {
        this.workType = workType;
    }

    /**
     * Information about the salary for the job.
     * (Required)
     * 
     */
    @JsonProperty("salary")
    public Salary getSalary() {
        return salary;
    }

    /**
     * Information about the salary for the job.
     * (Required)
     * 
     */
    @JsonProperty("salary")
    public void setSalary(Salary salary) {
        this.salary = salary;
    }

    /**
     * Description that is present in search results [limited to 150 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("jobSummary")
    public String getJobSummary() {
        return jobSummary;
    }

    /**
     * Description that is present in search results [limited to 150 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("jobSummary")
    public void setJobSummary(String jobSummary) {
        this.jobSummary = jobSummary;
    }

    /**
     * Full details of the job [limited to 20000 characters]. Basic formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("advertisementDetails")
    public String getAdvertisementDetails() {
        return advertisementDetails;
    }

    /**
     * Full details of the job [limited to 20000 characters]. Basic formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * (Required)
     * 
     */
    @JsonProperty("advertisementDetails")
    public void setAdvertisementDetails(String advertisementDetails) {
        this.advertisementDetails = advertisementDetails;
    }

    /**
     * name, phone, email
     * 
     */
    @JsonProperty("contact")
    public Contact getContact() {
        return contact;
    }

    /**
     * name, phone, email
     * 
     */
    @JsonProperty("contact")
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
     * An optional video related to the job and its postition within the advertisement. Provided link must be secure (HTTPS) to be accepted
     * 
     */
    @JsonProperty("video")
    public Video getVideo() {
        return video;
    }

    /**
     * An optional video related to the job and its postition within the advertisement. Provided link must be secure (HTTPS) to be accepted
     * 
     */
    @JsonProperty("video")
    public void setVideo(Video video) {
        this.video = video;
    }

    /**
     * Email applications directed to.
     * 
     */
    @JsonProperty("applicationEmail")
    public String getApplicationEmail() {
        return applicationEmail;
    }

    /**
     * Email applications directed to.
     * 
     */
    @JsonProperty("applicationEmail")
    public void setApplicationEmail(String applicationEmail) {
        this.applicationEmail = applicationEmail;
    }

    /**
     * The URL of the Job Application Form if not on SEEK [limited to 500 characters].
     * 
     */
    @JsonProperty("applicationFormUrl")
    public String getApplicationFormUrl() {
        return applicationFormUrl;
    }

    /**
     * The URL of the Job Application Form if not on SEEK [limited to 500 characters].
     * 
     */
    @JsonProperty("applicationFormUrl")
    public void setApplicationFormUrl(String applicationFormUrl) {
        this.applicationFormUrl = applicationFormUrl;
    }

    /**
     * The URL that the candidate lands on at the end of the application [limited to 500 characters].
     * 
     */
    @JsonProperty("endApplicationUrl")
    public String getEndApplicationUrl() {
        return endApplicationUrl;
    }

    /**
     * The URL that the candidate lands on at the end of the application [limited to 500 characters].
     * 
     */
    @JsonProperty("endApplicationUrl")
    public void setEndApplicationUrl(String endApplicationUrl) {
        this.endApplicationUrl = endApplicationUrl;
    }

    /**
     * The ID number of an existing SEEK Screen to attach to the job advertisement
     * 
     */
    @JsonProperty("screenId")
    public Integer getScreenId() {
        return screenId;
    }

    /**
     * The ID number of an existing SEEK Screen to attach to the job advertisement
     * 
     */
    @JsonProperty("screenId")
    public void setScreenId(Integer screenId) {
        this.screenId = screenId;
    }

    /**
     * A quotable reference code used by the advertiser to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("jobReference")
    public String getJobReference() {
        return jobReference;
    }

    /**
     * A quotable reference code used by the advertiser to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("jobReference")
    public void setJobReference(String jobReference) {
        this.jobReference = jobReference;
    }

    /**
     * An additional reference code used by the agent to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("agentJobReference")
    public String getAgentJobReference() {
        return agentJobReference;
    }

    /**
     * An additional reference code used by the agent to identify the job advertisement [limited to 50 characters]. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("agentJobReference")
    public void setAgentJobReference(String agentJobReference) {
        this.agentJobReference = agentJobReference;
    }

    /**
     * When a custom template is to be used for the advertisement, it's ID and custom field values can be specified here.
     * 
     */
    @JsonProperty("template")
    public Template getTemplate() {
        return template;
    }

    /**
     * When a custom template is to be used for the advertisement, it's ID and custom field values can be specified here.
     * 
     */
    @JsonProperty("template")
    public void setTemplate(Template template) {
        this.template = template;
    }

    /**
     * When the *advertisementType* is *StandOut* then this attribute contains standout advertisement values.
     * 
     */
    @JsonProperty("standout")
    public Standout getStandout() {
        return standout;
    }

    /**
     * When the *advertisementType* is *StandOut* then this attribute contains standout advertisement values.
     * 
     */
    @JsonProperty("standout")
    public void setStandout(Standout standout) {
        this.standout = standout;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("recruiter")
    public Recruiter getRecruiter() {
        return recruiter;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("recruiter")
    public void setRecruiter(Recruiter recruiter) {
        this.recruiter = recruiter;
    }

    /**
     * When *ResidentsOnly* is present, the job will specify that it is available for Australian/NZ residents only.
     * 
     */
    @JsonProperty("additionalProperties")
    public List<Object> getAdditionalProperties() {
        return additionalProperties;
    }

    /**
     * When *ResidentsOnly* is present, the job will specify that it is available for Australian/NZ residents only.
     * 
     */
    @JsonProperty("additionalProperties")
    public void setAdditionalProperties(List<Object> additionalProperties) {
        this.additionalProperties = additionalProperties;
    }

    /**
     * the ID of the advertisement. Can be used with link templates from the API links endpoint.
     * (Required)
     * 
     */
    @JsonProperty("id")
    public String getId() {
        return id;
    }

    /**
     * the ID of the advertisement. Can be used with link templates from the API links endpoint.
     * (Required)
     * 
     */
    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("expiryDate")
    public String getExpiryDate() {
        return expiryDate;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("expiryDate")
    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    /**
     * Value is one of:
     * (Required)
     * 
     */
    @JsonProperty("state")
    public SeekJobPostResponse.State getState() {
        return state;
    }

    /**
     * Value is one of:
     * (Required)
     * 
     */
    @JsonProperty("state")
    public void setState(SeekJobPostResponse.State state) {
        this.state = state;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("_links")
    public Links getLinks() {
        return links;
    }

    /**
     * 
     * (Required)
     * 
     */
    @JsonProperty("_links")
    public void setLinks(Links links) {
        this.links = links;
    }

    public enum AdvertisementType {

        STAND_OUT("StandOut"),
        CLASSIC("Classic");
        private final String value;
        private final static Map<String, SeekJobPostResponse.AdvertisementType> CONSTANTS = new HashMap<String, SeekJobPostResponse.AdvertisementType>();

        static {
            for (SeekJobPostResponse.AdvertisementType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private AdvertisementType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static SeekJobPostResponse.AdvertisementType fromValue(String value) {
            SeekJobPostResponse.AdvertisementType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum State {

        OPEN("Open"),
        EXPIRED("Expired");
        private final String value;
        private final static Map<String, SeekJobPostResponse.State> CONSTANTS = new HashMap<String, SeekJobPostResponse.State>();

        static {
            for (SeekJobPostResponse.State c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private State(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static SeekJobPostResponse.State fromValue(String value) {
            SeekJobPostResponse.State constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

    public enum WorkType {

        FULL_TIME("FullTime"),
        PART_TIME("PartTime"),
        CASUAL("Casual"),
        CONTRACT_TEMP("ContractTemp");
        private final String value;
        private final static Map<String, SeekJobPostResponse.WorkType> CONSTANTS = new HashMap<String, SeekJobPostResponse.WorkType>();

        static {
            for (SeekJobPostResponse.WorkType c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private WorkType(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static SeekJobPostResponse.WorkType fromValue(String value) {
            SeekJobPostResponse.WorkType constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
