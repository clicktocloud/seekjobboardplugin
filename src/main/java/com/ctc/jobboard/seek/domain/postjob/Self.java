
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "href"
})
public class Self {

    /**
     * The HAL link (a relative URI) for performing future operations on the advertisement. When creating an advertisement, this link should be stored so that future operations such as "get processing status", "update", "expire", etc. can be performed on the advertisement.
     * (Required)
     * 
     */
    @JsonProperty("href")
    @JsonPropertyDescription("The HAL link (a relative URI) for performing future operations on the advertisement. When creating an advertisement, this link should be stored so that future operations such as \"get processing status\", \"update\", \"expire\", etc. can be performed on the advertisement.")
    private String href;

    /**
     * The HAL link (a relative URI) for performing future operations on the advertisement. When creating an advertisement, this link should be stored so that future operations such as "get processing status", "update", "expire", etc. can be performed on the advertisement.
     * (Required)
     * 
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * The HAL link (a relative URI) for performing future operations on the advertisement. When creating an advertisement, this link should be stored so that future operations such as "get processing status", "update", "expire", etc. can be performed on the advertisement.
     * (Required)
     * 
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

}
