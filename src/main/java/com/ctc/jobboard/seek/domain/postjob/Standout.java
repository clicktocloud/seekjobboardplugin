
package com.ctc.jobboard.seek.domain.postjob;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * When the *advertisementType* is *StandOut* then this attribute contains standout advertisement values.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "logoId",
    "bullets"
})
public class Standout {

    /**
     * The Logo ID to display in the search results.
     * 
     */
    @JsonProperty("logoId")
    @JsonPropertyDescription("The Logo ID to display in the search results.")
    private Integer logoId;
    /**
     * Array of 3 bullet points to display. Each bullet point is limited to 80 characters. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("bullets")
    @JsonPropertyDescription("Array of 3 bullet points to display. Each bullet point is limited to 80 characters. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.")
    private List<Object> bullets = null;

    /**
     * The Logo ID to display in the search results.
     * 
     */
    @JsonProperty("logoId")
    public Integer getLogoId() {
        return logoId;
    }

    /**
     * The Logo ID to display in the search results.
     * 
     */
    @JsonProperty("logoId")
    public void setLogoId(Integer logoId) {
        this.logoId = logoId;
    }

    /**
     * Array of 3 bullet points to display. Each bullet point is limited to 80 characters. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("bullets")
    public List<Object> getBullets() {
        return bullets;
    }

    /**
     * Array of 3 bullet points to display. Each bullet point is limited to 80 characters. No formatting tags are allowed e.g. < b >Bold< /b >, < br >, etc.
     * 
     */
    @JsonProperty("bullets")
    public void setBullets(List<Object> bullets) {
        this.bullets = bullets;
    }

}
