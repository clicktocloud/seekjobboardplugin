
package com.ctc.jobboard.seek.domain.postjob;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * When a custom template is to be used for the advertisement, it's ID and custom field values can be specified here.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "items"
})
public class Template {

    /**
     * Defines the SEEK Template ID used for a job advertisement.
     * 
     */
    @JsonProperty("id")
    @JsonPropertyDescription("Defines the SEEK Template ID used for a job advertisement.")
    private Integer id;
    /**
     * An array of name-value pairs to specify values  of custom fields on  the custom template.
     * 
     */
    @JsonProperty("items")
    @JsonPropertyDescription("An array of name-value pairs to specify values  of custom fields on  the custom template.")
    private List<Object> items = null;

    /**
     * Defines the SEEK Template ID used for a job advertisement.
     * 
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * Defines the SEEK Template ID used for a job advertisement.
     * 
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * An array of name-value pairs to specify values  of custom fields on  the custom template.
     * 
     */
    @JsonProperty("items")
    public List<Object> getItems() {
        return items;
    }

    /**
     * An array of name-value pairs to specify values  of custom fields on  the custom template.
     * 
     */
    @JsonProperty("items")
    public void setItems(List<Object> items) {
        this.items = items;
    }

}
