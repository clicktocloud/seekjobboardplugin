
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


/**
 * Required when creating or updating an advertisement on behalf of an advertiser. Must not be supplied when advertiser creates or updates the advertisement as themselves. When this attribute is supplied then advertiserId is required. When this attribute is supplied then agentId should only be supplied when the caller is posting an advertisement on behalf of the agent i.e. the caller is not the agent.
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "advertiserId",
    "agentId"
})
public class ThirdParties {

    /**
     * Identity of the Client that this Advertisement is being posted for
     * (Required)
     * 
     */
    @JsonProperty("advertiserId")
    @JsonPropertyDescription("Identity of the Client that this Advertisement is being posted for")
    private String advertiserId;
    /**
     * Identity of the agent that is creating or updating an advertisement on behalf of an advertiser when the agent is posting the advertisement using a third party uploader.
     * 
     */
    @JsonProperty("agentId")
    @JsonPropertyDescription("Identity of the agent that is creating or updating an advertisement on behalf of an advertiser when the agent is posting the advertisement using a third party uploader.")
    private String agentId;

    /**
     * Identity of the Client that this Advertisement is being posted for
     * (Required)
     * 
     */
    @JsonProperty("advertiserId")
    public String getAdvertiserId() {
        return advertiserId;
    }

    /**
     * Identity of the Client that this Advertisement is being posted for
     * (Required)
     * 
     */
    @JsonProperty("advertiserId")
    public void setAdvertiserId(String advertiserId) {
        this.advertiserId = advertiserId;
    }

    /**
     * Identity of the agent that is creating or updating an advertisement on behalf of an advertiser when the agent is posting the advertisement using a third party uploader.
     * 
     */
    @JsonProperty("agentId")
    public String getAgentId() {
        return agentId;
    }

    /**
     * Identity of the agent that is creating or updating an advertisement on behalf of an advertiser when the agent is posting the advertisement using a third party uploader.
     * 
     */
    @JsonProperty("agentId")
    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

}
