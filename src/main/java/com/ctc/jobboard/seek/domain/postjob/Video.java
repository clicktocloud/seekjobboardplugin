
package com.ctc.jobboard.seek.domain.postjob;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.annotation.JsonValue;


/**
 * An optional video related to the job and its postition within the advertisement. Provided link must be secure (HTTPS) to be accepted
 * 
 */
@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "url",
    "position"
})
public class Video {

    /**
     * Full 'embed' link including object tags as displayed on YouTube [limited to 255 characters].
     * 
     */
    @JsonProperty("url")
    @JsonPropertyDescription("Full 'embed' link including object tags as displayed on YouTube [limited to 255 characters].")
    private String url;
    /**
     * Value must be one of:
     * 
     */
    @JsonProperty("position")
    @JsonPropertyDescription("Value must be one of:")
    private Video.Position position;

    /**
     * Full 'embed' link including object tags as displayed on YouTube [limited to 255 characters].
     * 
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * Full 'embed' link including object tags as displayed on YouTube [limited to 255 characters].
     * 
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * Value must be one of:
     * 
     */
    @JsonProperty("position")
    public Video.Position getPosition() {
        return position;
    }

    /**
     * Value must be one of:
     * 
     */
    @JsonProperty("position")
    public void setPosition(Video.Position position) {
        this.position = position;
    }

    public enum Position {

        ABOVE("Above"),
        BELOW("Below");
        private final String value;
        private final static Map<String, Video.Position> CONSTANTS = new HashMap<String, Video.Position>();

        static {
            for (Video.Position c: values()) {
                CONSTANTS.put(c.value, c);
            }
        }

        private Position(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return this.value;
        }

        @JsonValue
        public String value() {
            return this.value;
        }

        @JsonCreator
        public static Video.Position fromValue(String value) {
            Video.Position constant = CONSTANTS.get(value);
            if (constant == null) {
                throw new IllegalArgumentException(value);
            } else {
                return constant;
            }
        }

    }

}
