
package com.ctc.jobboard.seek.domain.postjob;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyDescription;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonIgnoreProperties(ignoreUnknown=true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "href"
})
public class View {

    /**
     * The HAL link (a relative URI) to the live advertisement on the SEEK website. When the advertisement has been created and its processing status is still "Pending", the SEEK website will indicate that the advertisement is not available.
     * (Required)
     * 
     */
    @JsonProperty("href")
    @JsonPropertyDescription("The HAL link (a relative URI) to the live advertisement on the SEEK website. When the advertisement has been created and its processing status is still \"Pending\", the SEEK website will indicate that the advertisement is not available.")
    private String href;

    /**
     * The HAL link (a relative URI) to the live advertisement on the SEEK website. When the advertisement has been created and its processing status is still "Pending", the SEEK website will indicate that the advertisement is not available.
     * (Required)
     * 
     */
    @JsonProperty("href")
    public String getHref() {
        return href;
    }

    /**
     * The HAL link (a relative URI) to the live advertisement on the SEEK website. When the advertisement has been created and its processing status is still "Pending", the SEEK website will indicate that the advertisement is not available.
     * (Required)
     * 
     */
    @JsonProperty("href")
    public void setHref(String href) {
        this.href = href;
    }

}
