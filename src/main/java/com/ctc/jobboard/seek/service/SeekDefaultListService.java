package com.ctc.jobboard.seek.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.ctc.jobboard.defaultlist.AdvertiserJobTemplateLogoList;
import com.ctc.jobboard.defaultlist.Area;
import com.ctc.jobboard.defaultlist.AreaList;
import com.ctc.jobboard.defaultlist.ClassificationList;
import com.ctc.jobboard.defaultlist.Country;
import com.ctc.jobboard.defaultlist.CountryList;
import com.ctc.jobboard.defaultlist.JobTemplateList;
import com.ctc.jobboard.defaultlist.Location;
import com.ctc.jobboard.defaultlist.LocationList;
import com.ctc.jobboard.defaultlist.SalaryRange;
import com.ctc.jobboard.defaultlist.SalaryRange.Band;
import com.ctc.jobboard.defaultlist.SalaryRange.Range;
import com.ctc.jobboard.defaultlist.SalaryType;
import com.ctc.jobboard.defaultlist.SalaryTypeList;
import com.ctc.jobboard.defaultlist.SubClassification;
import com.ctc.jobboard.defaultlist.SubClassificationList;
import com.ctc.jobboard.defaultlist.WorkType;
import com.ctc.jobboard.defaultlist.WorkTypeList;
import com.ctc.jobboard.seek.component.SeekSettings;
import com.ctc.jobboard.seek.domain.ext.Classification;
import com.ctc.jobboard.seek.domain.ext.ItemType;

public class SeekDefaultListService {
	
	public static  enum SalaryRangType{AU_NZ, UK};
	
	public static final SalaryRangType SALARY_RANGE_AU_NZ = SalaryRangType.AU_NZ;
	public static final SalaryRangType SALARY_RANGE_UK = SalaryRangType.UK;
	public static final String HOURLY_SALARY = "hour";
	
	

	public JobTemplateList getJobTemplateList(){
		JobTemplateList list = new JobTemplateList();
		//TODO
		
		return list;
	}
	
	public AdvertiserJobTemplateLogoList getAdvertiserJobTemplateLogoList(){
		AdvertiserJobTemplateLogoList list = new AdvertiserJobTemplateLogoList();
		//TODO
		
		return list;
	}
	
	public WorkTypeList getWorkTypeList(){
		WorkTypeList list = new WorkTypeList();
		Collection<ItemType> workTypes = SeekSettings.getWorkTypes();
		if( workTypes != null){
			for(ItemType type : workTypes){
				list.getWorkType().add(new WorkType(type.getId(),type.getDescription()));
			}

		}
		
		return list;
	}
	
	public SalaryTypeList getSalaryTypeList(){
		
		SalaryTypeList list = new SalaryTypeList();
		Collection<ItemType> types = SeekSettings.getSalaryTypes();
		if(types != null){
			for(ItemType type  :types){
				SalaryType st = new SalaryType(type.getId(), type.getDescription());
				
				SalaryRange sg = getSalaryRange(SalaryRangType.AU_NZ);
				List<Range> annuals = new ArrayList<Range>();
				List<Range> hourlys = new ArrayList<Range>();
				for(Band band : sg.getBands()){
					if(type.getId().toLowerCase().contains(HOURLY_SALARY)){
						hourlys.addAll(band.getHourlyRanges());
					}else{
						annuals.addAll( band.getAnnualRanges());
					}	
				}
				
				st.setHourlyRanges(hourlys);
				st.setAnnualRanges(annuals);
				list.getSalaryType().add( st );
			}
		}
		
		return list;
		
	}
	
	public SalaryRange getSalaryRange(SalaryRangType type){
		if(SalaryRangType.AU_NZ == type){
			return SeekSettings.getAuSalaryRange();
		}else if(SalaryRangType.UK == type){
			return SeekSettings.getUkSalaryRange();
		}else{
			return null;
		}
		
	}
	
	
	public CountryList getCountryList(){
		CountryList countryList = new CountryList();
		Collection<com.ctc.jobboard.seek.domain.ext.Location> nations = SeekSettings.getNations();
		if( nations != null){
			for(com.ctc.jobboard.seek.domain.ext.Location nation : nations){
				Country country = new Country(nation.getId(), nation.getDescription());
				countryList.getCountries().add(country);
				LocationList locationList = getLocationList(country.getCountryId());
				country.setLocationList(locationList);
				
			}			
		}
		
		
		
		return countryList;
	}
	
	public LocationList getLocationList(){
		return getLocationList("");
	}
	
	public LocationList getLocationList(String countryId){
		LocationList locationList = new LocationList();
		
		List<com.ctc.jobboard.seek.domain.ext.Location> states = SeekSettings.getStates() ;
		if( states != null){
			
			Collections.sort(states, SeekSettings.ITEM_COMPARATOR);
			

			for(com.ctc.jobboard.seek.domain.ext.Location state : states){
				if(!StringUtils.isEmpty(countryId) && ! countryId.equals(state.getParentId())){
					continue;
				}
				Location stateLocation = new Location("$-"+state.getId(), state.getDescription(), state.getParentId());
				
				locationList.getLocation().add(stateLocation);
				
				List<com.ctc.jobboard.seek.domain.ext.Location> locations = state.getChildren();
				if(locations != null){
					
					Collections.sort(locations, SeekSettings.ITEM_COMPARATOR);
					
					for(com.ctc.jobboard.seek.domain.ext.Location location : locations){
						
						Location countryLocation = new Location(location.getId(), location.getDescription(), state.getParentId());
						
						locationList.getLocation().add(countryLocation);
						
						
						List<com.ctc.jobboard.seek.domain.ext.Location> areas = location.getChildren();
						if(areas != null){
							
							Collections.sort(areas, SeekSettings.ITEM_COMPARATOR);
							
							for(com.ctc.jobboard.seek.domain.ext.Location area : areas){
								if(countryLocation.getAreaList() == null){
									countryLocation.setAreaList(new AreaList());
								}
								
								countryLocation.getAreaList().getArea().add(new Area(area.getId(), area.getDescription(), countryLocation.getLocationId(), countryLocation.getCountryId()));
							}
						}
					}
				}						
			}
		
		}
		
		
		
		return locationList;
	}
	

	public AreaList getAreaList(){
		
		
		return getAreaList("");
	}
	
	public AreaList getAreaList(String locationId){
		
		AreaList areaList = new AreaList();
		
		LocationList locationList = getLocationList();
		for(Location l : locationList.getLocation()){
			if(!StringUtils.isEmpty(locationId) && !locationId.equals(l.getLocationId())){
				continue;
			}
			if(l.getAreaList() != null ){
				areaList.getArea().addAll(l.getAreaList().getArea());
			}
		}
		
		
		return areaList;
	}
	
	public ClassificationList getClassificationList(){
		ClassificationList list = new ClassificationList();
		List<Classification> cs = SeekSettings.getClassifications();
		if(cs != null){
			
			Collections.sort(cs, SeekSettings.ITEM_COMPARATOR);
			
			for(Classification c : cs){
				com.ctc.jobboard.defaultlist.Classification classification  = new com.ctc.jobboard.defaultlist.Classification(c.getId(), c.getDescription());
				list.getClassification().add(classification);
				
				SubClassificationList subList = getSubClassificationList(classification.getId());
				
				classification.setSubClassificationList(subList);
			}
		}
		
		return list;
	}
	
	public SubClassificationList getSubClassificationList(){
		
		return getSubClassificationList("");
	}
	
	public SubClassificationList getSubClassificationList(String parentId){
		SubClassificationList list = new SubClassificationList();
		List<Classification> subs = SeekSettings.getSubClassifications();
		
		
		
		if(subs!= null){
			
			Collections.sort(subs, SeekSettings.ITEM_COMPARATOR);
			
			for( Classification c : subs){
				if(!StringUtils.isEmpty(parentId) && !parentId.equals(c.getParentId())){
					continue;
				}
				
				list.getSubClassification().add(new SubClassification(c.getId(), c.getDescription(),c.getParentId()));
			}	
		}
		
		return list;
	}


}
