package com.ctc.jobboard.seek.service;

import java.util.List;

import com.ctc.jobboard.core.JobBoardCredential;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.defaultlist.DefaultList;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.plugin.JobBoardPluginInterface;
import com.ctc.jobboard.seek.component.SeekResponsePackage;
import com.ctc.jobboard.seek.component.SeekSettings;
import com.ctc.jobboard.seek.domain.alljobs.Advertisement;
import com.ctc.jobboard.seek.domain.alljobs.SeekAdvertismentsList;

public class SeekJobBoardPlugin implements JobBoardPluginInterface {
	
	static DefaultList defaultList = null;
	
	public SeekJobBoardPlugin(String boardName) {
		super();
		this.boardName = boardName;
		
	}

	private String boardName;
	
	@Override
	public void setJobBoardName(String boardName) {
		this.boardName = boardName;
		
	}

	

	@Override
	public JobBoardResponsePackage createJob(String referenceNo, String postingStatus,
			String jobJsonContent, JobBoardCredential credential) throws JobBoardException{
		SeekPostJobService service = new SeekPostJobService(boardName);
		
		service.createJob(credential.getOrgId(), referenceNo, postingStatus, jobJsonContent, credential.getAdvertiserId(), credential.getNamespace());
		return service.getResponsePackage();
	}

	@Override
	public JobBoardResponsePackage updateJob(String referenceNo, String seekAdId,
			String postingStatus, String jobJsonContent,
			JobBoardCredential credential) throws JobBoardException{
		
		SeekPostJobService service = new SeekPostJobService(boardName);
		service.updateJob(credential.getOrgId(), referenceNo, seekAdId,postingStatus, jobJsonContent, credential.getAdvertiserId(),  credential.getNamespace());
		return service.getResponsePackage();
		
	}

	@Override
	public JobBoardResponsePackage archiveJob(String referenceNo, String seekAdId,
			String postingStatus, JobBoardCredential credential) throws JobBoardException{
		
		SeekPostJobService service = new SeekPostJobService(boardName);
		service.archiveJob(credential.getOrgId(), referenceNo, seekAdId,postingStatus,  credential.getAdvertiserId(),  credential.getNamespace());
		
		return service.getResponsePackage();
	}

	@Override
	public JobBoardResponsePackage getAllJobs(JobBoardCredential credential) throws JobBoardException{
		
		SeekRetrieveAdvertisementService service = new SeekRetrieveAdvertisementService(boardName);
		List<Advertisement> ads = service.getAllJobs(credential.getAdvertiserId(), credential.getClientId(), credential.getClientSecret());
		
		return new SeekResponsePackage(new SeekAdvertismentsList(ads), "");
		
	}

	@Override
	public JobBoardResponsePackage getJob(String seekAdId,
			JobBoardCredential credential) throws JobBoardException {
		
		SeekRetrieveAdvertisementService service = new SeekRetrieveAdvertisementService(boardName);
		return  new SeekResponsePackage(service.getJob(seekAdId, credential.getClientId(), credential.getClientSecret()), "");
		
	}



	@Override
	public synchronized DefaultList getDefaultList(JobBoardCredential arg0) {
		
		
			if (SeekSettings.isChanged() || defaultList == null) {
				defaultList = new DefaultList();
				SeekDefaultListService service = new SeekDefaultListService();
				defaultList.setClassificationList(service
						.getClassificationList());
				defaultList.setCountryList(service.getCountryList());
				defaultList.setAdvertiserJobTemplateLogoList(service
						.getAdvertiserJobTemplateLogoList());
				defaultList.setJobTemplateList(service.getJobTemplateList());
				defaultList
						.setSalaryRange(service
								.getSalaryRange(SeekDefaultListService.SalaryRangType.AU_NZ));
				defaultList.setSalaryTypeList(service.getSalaryTypeList());
				defaultList.setWorkTypeList(service.getWorkTypeList());
				
				SeekSettings.setChanged( false );
			}
			return defaultList;
		
	}

	

}
