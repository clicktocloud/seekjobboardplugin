package com.ctc.jobboard.seek.service;



import javax.xml.bind.JAXBException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.seek.component.SeekJobPostBoards;
import com.ctc.jobboard.seek.domain.SeekBaseRequest;
import com.ctc.jobboard.seek.domain.SeekBaseResponse;
import com.ctc.jobboard.seek.domain.postjob.SeekJobExpiredRequest;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostRequest;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;
import com.ctc.jobboard.seek.utils.SeekConfig;
import com.ctc.jobboard.util.SfOrg;

public class SeekPostJobService extends SeekJobPostBoards{
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	private boolean enableCleanseAdvertisementDetails ;
	
	public SeekPostJobService() {
		super();
		
	}
	
	public SeekPostJobService(String jobBoardName) {
		super(jobBoardName);
		
	}
	
	public SeekPostJobService(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
		
	}
	
	public SeekBaseResponse createJob(String orgId, String referenceNo, String postingStatus, String jobJsonContent,String advertiserId, String namespace) throws JobBoardException{
		
		
		
		return createJob(orgId, referenceNo,postingStatus, jobJsonContent, advertiserId, SeekConfig.getSeekUserName(),SeekConfig.getSeekPassword(),namespace);
	
	}
	
	public SeekBaseResponse createJob(String orgId, String referenceNo, String postingStatus, String jobJsonContent,String advertiserId, String username, String password, String namespace) throws JobBoardException{
		
		setNamespace(namespace);
		
		setAccount( buildAccount( advertiserId, username, password));
		
		return createJob(orgId, referenceNo,postingStatus, jobJsonContent);
	
	}
	
	public SeekBaseResponse createJob(String orgId, String referenceNo, String postingStatus, String jobJsonContent) {
		
		currentSforg = new SfOrg(orgId);
		this.actionType = JobBoard.INSERT;
		
		String createUrl = getAdvertisementPath();
		if(StringUtils.isEmpty(createUrl)){
			createUrl = requestEndpoints.get(SeekJobPostRequest.class);
		}
		logger.debug(createUrl);
		return postJob( orgId,  referenceNo,  postingStatus,  jobJsonContent, createUrl) ;
	
	
	}
	
	public SeekBaseResponse updateJob(String orgId, String referenceNo,String seekAdId, String postingStatus, String jobJsonContent,String advertiserId,  String namespace) throws JobBoardException{
		
		
		return updateJob(orgId, referenceNo,seekAdId, postingStatus, jobJsonContent,advertiserId, SeekConfig.getSeekUserName(),SeekConfig.getSeekPassword(),namespace);
	
	}

	public SeekBaseResponse updateJob(String orgId, String referenceNo,String seekAdId, String postingStatus, String jobJsonContent,String advertiserId, String username, String password, String namespace) throws JobBoardException{
		
		setNamespace(namespace);
		
		setAccount( buildAccount(advertiserId, username, password));
		
		
		
		return updateJob(orgId, referenceNo,seekAdId, postingStatus, jobJsonContent);
	
	}
	
	public SeekBaseResponse updateJob(String orgId, String referenceNo, String seekAdId, String postingStatus, String jobJsonContent) {
		
		this.actionType = JobBoard.UPDATE;
		currentSforg = new SfOrg(orgId);
		
		String updateUrl = getAdvertisementLink(seekAdId);
		if(StringUtils.isEmpty(updateUrl)){
			updateUrl = requestEndpoints.get(SeekJobPostRequest.class) + "/"+ seekAdId;
		}
		
		return postJob( orgId,  referenceNo,  postingStatus,  jobJsonContent, updateUrl) ;
	
		
	}
	
	public SeekBaseResponse updateJobByUrl(String orgId, String referenceNo, String postingStatus, String jobJsonContent, String updateUrl) {
		
		this.actionType = JobBoard.UPDATE;
		currentSforg = new SfOrg(orgId);
		
		
		return postJob( orgId,  referenceNo,  postingStatus,  jobJsonContent, updateUrl) ;
	
		
	}
	
	public SeekBaseResponse archiveJob(String orgId, String referenceNo,String seekAdId, String postingStatus,String advertiserId,String namespace) throws JobBoardException{
		
	
		
		return archiveJob(orgId, referenceNo,seekAdId, postingStatus,advertiserId, SeekConfig.getSeekUserName(),SeekConfig.getSeekPassword(),namespace);
	
	}
	
	public SeekBaseResponse archiveJob(String orgId, String referenceNo,String seekAdId, String postingStatus,String advertiserId, String username, String password, String namespace) throws JobBoardException{
		
		setNamespace(namespace);
		
		setAccount( buildAccount( advertiserId,username, password));
		
		
		
		return archiveJob(orgId, referenceNo,seekAdId, postingStatus);
	
	}
	
	public SeekBaseResponse archiveJob(String orgId, String referenceNo, String seekAdId, String postingStatus) {
		
		this.actionType = JobBoard.ARCHIVE;
		currentSforg = new SfOrg(orgId);
		
		String updateUrl = getAdvertisementLink(seekAdId);
		if(StringUtils.isEmpty(updateUrl)){
			updateUrl = requestEndpoints.get(SeekJobExpiredRequest.class) + "/"+ seekAdId;
		}
		
		return postJob( orgId,  referenceNo,  postingStatus,  "", updateUrl) ;
	
		
	}
	
	public SeekBaseResponse postJob(String orgId, String referenceNo, String postingStatus, String jobJsonContent,String url) {
		
		
		try {
			this.referenceNo =  referenceNo;
			this.url = url;
			SeekBaseRequest request = null;
			
			if(JobBoard.ARCHIVE.equals(this.actionType)){
				request = new SeekJobExpiredRequest();
			}else{
				request = MarshallerHelper.convertJsontoObject(jobJsonContent, SeekJobPostRequest.class);
				if(enableCleanseAdvertisementDetails){
					((SeekJobPostRequest) request).setProcessingOptions(new String[]{"CleanseAdvertisementDetails"});
				}
			}
			 
			postJob( orgId,  referenceNo,  postingStatus,  request) ;
			
		} catch (JAXBException e) {
			//e.printStackTrace();
			logger.error("JAXBException - Invalid job content !", e);
			
			SeekJobPostResponse response = createJobPostResponse("Invalid job content","ERROR", "Invalid job content", this.referenceNo);
			
			setResponse(response);
			
			logger.debug("Begin to handler response");
			
			responseHandler();
			
			logger.debug("-----------------------------------------");
		} 
		
		
		
		return getResponse();
	
	}
	
	
	
	public SeekBaseResponse postJob(String orgId, String referenceNo, String postingStatus, SeekBaseRequest request) throws JobBoardException{
		if(orgId == null || referenceNo==null || request == null){
			logger.error("Org Id and job content can not be empty !");
			throw new JobBoardException("Org Id, reference No and job content can not be empty !");
		}

		
		
		this.request = request;
		
		try {

			String aid = SeekJobPostBoards.getAdId(referenceNo);
			if(! StringUtils.isEmpty( aid )){
				adPostingStatuses.put(aid, postingStatus);
			}
			
			makeJobFeeds();
			
		}  catch (JobBoardException e){
			logger.error("JobBoard Exception - ", e);
			
			SeekJobPostResponse response = createJobPostResponse(e.getMessage(),"ERROR", e.getMessage(), this.referenceNo);
			
			setResponse(response);
		}
		
		
		logger.debug("Begin to handler response");
		
		responseHandler();
		
		logger.debug("-----------------------------------------");
		
		return getResponse();
	
	}

	@Override
	public void makeJobFeeds() throws JobBoardException{
		if( request == null){
			return;
			
		}
		try {
			

			logger.debug("Begin to make job listing ["+referenceNo+"] for org ["+currentSforg.getOrgId()+"]");
			
			
			
			logger.debug("-----------------------------------------");
			
			logger.debug("Connecting to Salesforce instance of org ["+currentSforg.getOrgId()+"]");
			
			currentConnection = JBConnectionManager.connect( currentSforg.getOrgId() );
			if(currentConnection == null){
				logger.warn("No connection to Salesforce is created !");
			}
			
			logger.debug("-----------------------------------------");
			
			//
			logger.debug("Begin to post job ["+referenceNo+"] to job board["+jobBoardName+"]");
			
			post2JobBoard();
			
			logger.debug("-----------------------------------------");
			
			throwJxtExceptionFromUnsuccessfulJobPostResponse();
			
		}  catch (JobBoardException e) {
			logger.error(e);
			throw e;
		} catch (Exception e) {
			logger.error("Exception :referenceNo="+referenceNo+" orgId="+currentSforg.getOrgId()+"  "+e);
			throw new JobBoardException("Other error - ",e);
		}
	}

	public boolean isEnableCleanseAdvertisementDetails() {
		return enableCleanseAdvertisementDetails;
	}

	public void setEnableCleanseAdvertisementDetails(
			boolean enableCleanseAdvertisementDetails) {
		this.enableCleanseAdvertisementDetails = enableCleanseAdvertisementDetails;
	}

}
