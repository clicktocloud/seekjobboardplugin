package com.ctc.jobboard.seek.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.component.IAccount;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.exception.AccountException;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.seek.component.SeekJobBoard;
import com.ctc.jobboard.seek.domain.alljobs.Advertisement;
import com.ctc.jobboard.seek.domain.alljobs.SeekGetAllAdvertismentsRequest;
import com.ctc.jobboard.seek.domain.alljobs.SeekGetAllAdvertismentsResponse;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;
import com.ctc.jobboard.seek.utils.SeekConfig;

public class SeekRetrieveAdvertisementService extends SeekJobBoard {
	
	
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	public SeekRetrieveAdvertisementService( ) {
		super( );
		
	}
	
	public SeekRetrieveAdvertisementService(String jobBoardName) {
		super(jobBoardName);
		
	}
	
	public SeekRetrieveAdvertisementService(String jobBoardName, String jobBoardSfApiName) {
		super(jobBoardName, jobBoardSfApiName);
		
	}
	
	public IAccount getAccount( ) throws AccountException{
		return super.getAccount();
	}
	
	public List<Advertisement> getAllJobs( String advertiserId) throws JobBoardException{
		return getAllJobs(advertiserId, SeekConfig.getSeekUserName(), SeekConfig.getSeekPassword());
	}
	
	public List<Advertisement> getAllJobs( String advertiserId, String username, String password) throws JobBoardException{
		
		logger.debug("Begin to get all advertisments of advertiser [ " + (StringUtils.isEmpty( advertiserId) ? "ALL":advertiserId)  + " ]");
		
		List<Advertisement> ads = new ArrayList<Advertisement>();
		
		try {
		
			setAccount( buildAccount(advertiserId, username, password));
			
			String url = getAdvertisementsLink(advertiserId);
			if(StringUtils.isEmpty(url)){
				url = requestEndpoints.get(SeekGetAllAdvertismentsRequest.class);
				if(! StringUtils.isEmpty( url ) && !StringUtils.isEmpty(advertiserId)){
					url += "?advertiserId="+advertiserId;
				}
			}
			
			int pages = 1;
			while(!StringUtils.isEmpty( url )){
				logger.info("Retrieving advertisments from page ["+(pages ++ )+"] : " + url);
				SeekGetAllAdvertismentsResponse response = getJobsOfOnePage(url, username, password);
				if(response != null ){
					//retrieve the list of ads
					if(response.getEmbedded() != null && response.getEmbedded().getAdvertisements() != null ){
						ads.addAll(response.getEmbedded().getAdvertisements());
						
					}
					
					//retrieve the next page url
					if(response.getLinks() != null && response.getLinks().getNext() != null && response.getLinks().getNext().getHref() != null){
						url = SeekJobBoard.SERVICE_DOMAIN + response.getLinks().getNext().getHref();
					}else{
						url = "";
					}
					
				}
				
				Thread.sleep(1000);
				
				
			}
			
			logger.info("Finished - Total advertisements ["+ads.size()+"] of advertiser["+advertiserId+"]");
			
		} catch (AccountException e) {
			
			throw new JobBoardException("AccountException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JobBoardException("Other error - " + e.getMessage());
		}
		
		return ads;
	
	}
	
	public SeekGetAllAdvertismentsResponse getJobsOfOnePage( String pageUrl) throws JobBoardException{
		return getJobsOfOnePage(pageUrl, SeekConfig.getSeekUserName(), SeekConfig.getSeekPassword());
	}
	
	public SeekGetAllAdvertismentsResponse getJobsOfOnePage( String pageUrl,String username, String password) throws JobBoardException{
		
		logger.debug("Begin to get advertisments from "+ pageUrl);
		
		try {
		
			setAccount( buildAccount("",username, password));
			
			
			JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
			requestPackage.setRequest(new SeekGetAllAdvertismentsRequest());
			requestPackage.setMethodType(MethodType.GET);
			requestPackage.setUrl( pageUrl );
			
			requestPackage.addHeader("Accept", SeekJobBoard.ACCEPT_ALLJOBS);
			
		
			return execute(requestPackage, SeekGetAllAdvertismentsResponse.class);
			
		} catch (AccountException e) {
			
			throw new JobBoardException("AccountException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JobBoardException("Other error - " + e.getMessage());
		}
		
	
	}
	
	
	public SeekJobPostResponse getJob( String seekAdId) throws JobBoardException{
		return getJob(seekAdId, SeekConfig.getSeekUserName(), SeekConfig.getSeekPassword());
	}
	
	public SeekJobPostResponse getJob( String seekAdId, String username, String password) throws JobBoardException{
		
		logger.debug("Begin to get one advertisment["+seekAdId+"] ");
				try {
		
			setAccount( buildAccount("",username, password));
			
			String url = getAdvertisementLink(seekAdId);
			if(StringUtils.isEmpty(url)){
				url = requestEndpoints.get(SeekGetAllAdvertismentsRequest.class);
				url = url + "/"+seekAdId;
			}
			
			JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
			requestPackage.setRequest(new SeekGetAllAdvertismentsRequest());
			requestPackage.setMethodType(MethodType.GET);
			requestPackage.setUrl( url );
			requestPackage.addHeader("Accept", SeekJobBoard.ACCEPT);
			
			return execute(requestPackage, SeekJobPostResponse.class);
			
		} catch (AccountException e) {
			
			throw new JobBoardException("AccountException - " + e.getMessage());
		} catch( Exception e){
			
			throw new JobBoardException("Other error - " + e.getMessage());
		}
		
	
	}
	
	
	@Override
	public void makeJobFeeds() {
		
		
	}

	@Override
	public void responseHandler() {
		
	}

	@Override
	public void post2JobBoard() {
		
		
	}

}
