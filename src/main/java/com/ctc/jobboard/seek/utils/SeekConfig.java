package com.ctc.jobboard.seek.utils;

import org.apache.commons.lang.StringUtils;

import com.ctc.common.encryption.coder.AESCoder;
import com.ctc.jobboard.util.BasicConfig;

public class SeekConfig {
	
	private static String decryptedSeekUsername = "";
	private static String decryptedSeekPassword ="";
	
	public static String getSeekUserName(){
		synchronized (decryptedSeekUsername) {
			if (StringUtils.isEmpty(decryptedSeekUsername)) {
				decryptedSeekUsername = AESCoder.decrypt(BasicConfig.get("seek_username"));
			}
			return decryptedSeekUsername;
		}
	}
	
	public static String getSeekPassword(){
		
		synchronized (decryptedSeekPassword) {
			if (StringUtils.isEmpty(decryptedSeekPassword)) {
				decryptedSeekPassword = AESCoder.decrypt(BasicConfig.get("seek_password"));
			}
			return decryptedSeekPassword;
		}
		
	}

}
