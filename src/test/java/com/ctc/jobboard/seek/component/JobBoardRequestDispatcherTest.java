package com.ctc.jobboard.seek.component;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.core.JobBoardRequestDispatcher;
import com.ctc.jobboard.core.JobBoardRequestPackage;
import com.ctc.jobboard.core.JobBoardRequestPackage.MethodType;
import com.ctc.jobboard.core.JobContentJsonConverter;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.seek.domain.SeekBaseResponse;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostRequest;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;
import com.ctc.jobboard.util.JobBoardHelper;

public class JobBoardRequestDispatcherTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testExecute() throws IOException, BadRequestException, JAXBException {
		
		SeekAccount account = new SeekAccount(SeekOAuth.TEST_CLIENT_ID,SeekOAuth.TEST_CLIENT_SECRET,SeekOAuth.TEST_ADVERTISER_ID);
		
		SeekOAuth oauth = new SeekOAuth();
		String token = oauth.retrieveAccessToken(account);
		System.out.println(token);
		//String token = "320fa181-4a9a-4e24-ab6f-86efbbca333d";
		
		String bodyString = JobBoardHelper.readFileToString("/SeekNewJob.json");
		
		SeekJobPostRequest request = MarshallerHelper.convertJsontoObject(bodyString, SeekJobPostRequest.class);
		
		JobBoardRequestPackage requestPackage = new JobBoardRequestPackage();
		requestPackage.setMethodType(MethodType.POST);
		requestPackage.addHeader("Content-Type", SeekJobBoard.CONTENT_TYPE_JSON);
		requestPackage.addHeader("Accept", SeekJobBoard.ACCEPT);
		requestPackage.addHeader("Authorization", "Bearer "+ token);
		requestPackage.setRequest(request);
		
		requestPackage.setUrl("https://adposting-integration.cloud.seek.com.au/advertisement");
		
		JobBoardRequestDispatcher service  = new JobBoardRequestDispatcher();
		SeekBaseResponse response = service.execute(requestPackage, SeekJobPostResponse.class, new JobContentJsonConverter());
		
		System.out.println(MarshallerHelper.convertObjecttoJson(response));
		

		
	}

}
