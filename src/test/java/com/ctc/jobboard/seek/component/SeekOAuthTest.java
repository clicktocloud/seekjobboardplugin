package com.ctc.jobboard.seek.component;

import static org.junit.Assert.assertNotNull;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class SeekOAuthTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testRetrieveAccessToken() {
		SeekAccount account = new SeekAccount(SeekOAuth.TEST_CLIENT_ID,SeekOAuth.TEST_CLIENT_SECRET,SeekOAuth.TEST_ADVERTISER_ID);
		
		SeekOAuth oauth = new SeekOAuth();
		String token = oauth.retrieveAccessToken(account);
		System.out.println(token);
		assertNotNull(token);
		
	}

}
