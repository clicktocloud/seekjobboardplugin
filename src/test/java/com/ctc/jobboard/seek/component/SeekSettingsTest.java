package com.ctc.jobboard.seek.component;

import static org.junit.Assert.assertNotNull;

import java.util.Collection;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.defaultlist.SalaryRange;
import com.ctc.jobboard.seek.domain.ext.Classification;
import com.ctc.jobboard.seek.domain.ext.ItemType;
import com.ctc.jobboard.seek.domain.ext.Location;

public class SeekSettingsTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testLoadClassifications() {
		Collection<Classification> cs = SeekSettings.loadClassifications();
		
		assertNotNull(cs);
		
		for(Classification c : cs){
			System.out.println(c.getId()+ " - " + c.getDescription());
		}
	}
	
	@Ignore
	@Test
	public void testLoadLocations() {
		Collection<Location> nations = SeekSettings.loadLocations();
		
		assertNotNull(nations);
		
		for(Location n : nations){
			System.out.println(n.getId()+ " - " + n.getDescription());
			for(Location s : n.getChildren()){
				System.out.println("\t" + s.getId()+ " - " + s.getDescription());
				for(Location l : s.getChildren()){
					System.out.println("\t\t" + l.getId()+ " - " + l.getDescription());
				}
			}
			
		}
	}
	
	@Ignore
	@Test
	public void testLoadLocationsAndAreas() {
		Collection<Location> nations = SeekSettings.loadLocations();
		
		assertNotNull(nations);
		
		SeekSettings.loadAreas();
		
		for(Location n : nations){
			System.out.println(n.getId()+ " - " + n.getDescription());
			for(Location s : n.getChildren()){
				System.out.println("\t" + s.getId()+ " - " + s.getDescription());
				for(Location l : s.getChildren()){
					System.out.println("\t\t" + l.getId()+ " - " + l.getDescription());
					if(l.getChildren() != null){
						for(Location a : l.getChildren()){
							System.out.println("\t\t\t" + a.getId()+ " - " + a.getDescription());
						}
					}
					
				}
			}
			
		}
	}
	
	
	@Ignore
	@Test
	public void testLoadItemTypes() {
		Collection<ItemType> workTypes = SeekSettings.loadWorkTypes();
		
		assertNotNull(workTypes);
		
		System.out.println("Work Types ----------------------");
		for(ItemType type : workTypes){
			System.out.println(type.getId()+ " - " + type.getDescription());
			
		}
		
		Collection<ItemType> salaryTypes = SeekSettings.loadSalaryTypes();
		
		assertNotNull(salaryTypes);
		
		System.out.println("Salary Types ----------------------");
		
		for(ItemType type : salaryTypes){
			System.out.println(type.getId()+ " - " + type.getDescription());
			
		}
		
		
	}
	
	@Ignore
	@Test
	public void testLoadSalaryRange() {
		SalaryRange auRange  = SeekSettings.loadAuSalaryRange();
		
		assertNotNull(auRange);
		
		System.out.println("AU & NZ Salary Range ----------------------");
		System.out.println(auRange.getName());
		for(SalaryRange.Band band : auRange.getBands()){
			System.out.println(band.getName() + " : ");
			System.out.println("Annual:");
			for(SalaryRange.Range range : band.getAnnualRanges()){
				System.out.println("\t " + range.getFrom() + " --> " + range.getTo());
				
			}
			
			System.out.println("Hourly:");
			for(SalaryRange.Range range : band.getHourlyRanges()){
				System.out.println("\t " + range.getFrom() + " --> " + range.getTo());
				
			}
			
		}
		
		SalaryRange ukRange  = SeekSettings.loadUkSalaryRange();
		
		assertNotNull(ukRange);
		
		System.out.println("UK Salary Range ----------------------");
		for(SalaryRange.Band band : ukRange.getBands()){
			System.out.println(band.getName() + " : ");
			System.out.println("Annual:");
			for(SalaryRange.Range range : band.getAnnualRanges()){
				System.out.println("\t " + range.getFrom() + " --> " + range.getTo());
				
			}
			
			System.out.println("Hourly:");
			for(SalaryRange.Range range : band.getHourlyRanges()){
				System.out.println("\t " + range.getFrom() + " --> " + range.getTo());
				
			}
			
		}
		
		
	}

}
