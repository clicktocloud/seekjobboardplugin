package com.ctc.jobboard.seek.service;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.marshaller.MarshallerHelper;

public class SeekDefaultListServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testGetWorkTypeList() throws JAXBException {
		SeekDefaultListService service = new SeekDefaultListService();
		System.out.println(MarshallerHelper.convertObjecttoJson(service.getWorkTypeList()));
	}
	
	
	@Test
	public void testGetCountryList() throws JAXBException {
		SeekDefaultListService service = new SeekDefaultListService();
		System.out.println(MarshallerHelper.convertObjecttoJson(service.getCountryList()));
	}
	
	@Ignore
	@Test
	public void testGetClassificationList() throws JAXBException {
		SeekDefaultListService service = new SeekDefaultListService();
		System.out.println(MarshallerHelper.convertObjecttoJson(service.getClassificationList()));
	}
	
	@Ignore
	@Test
	public void testGetSubClassificationList() throws JAXBException {
		SeekDefaultListService service = new SeekDefaultListService();
		System.out.println(MarshallerHelper.convertObjecttoJson(service.getSubClassificationList()));
	}
	

	@Ignore
	@Test
	public void testGetSalaryTypeList() throws JAXBException {
		SeekDefaultListService service = new SeekDefaultListService();
		System.out.println(MarshallerHelper.convertObjecttoJson(service.getSalaryTypeList()));
	}
	
	@Ignore
	@Test
	public void testGetSalaryRange_AU() throws JAXBException {
		SeekDefaultListService service = new SeekDefaultListService();
		System.out.println(MarshallerHelper.convertObjecttoJson(service.getSalaryRange(SeekDefaultListService.SalaryRangType.UK)));
	}

}
