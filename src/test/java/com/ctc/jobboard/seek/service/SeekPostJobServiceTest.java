package com.ctc.jobboard.seek.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.component.IAccount;
import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardCredential;
import com.ctc.jobboard.exception.BadRequestException;
import com.ctc.jobboard.plugin.JobBoardPluginInterface;
import com.ctc.jobboard.seek.component.SeekAccount;
import com.ctc.jobboard.seek.component.SeekOAuth;
import com.ctc.jobboard.seek.domain.apilinks.Links;
import com.ctc.jobboard.util.JobBoardHelper;

public class SeekPostJobServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testGetAccessToken() {
		SeekPostJobService service = new SeekPostJobService("Seek");
		
		IAccount account = new SeekAccount(SeekOAuth.TEST_CLIENT_ID, SeekOAuth.TEST_CLIENT_SECRET, SeekOAuth.TEST_ADVERTISER_ID);
		service.setAccount(account);
		
		String token1 = service.getAccessToken();
		
		String token2 = service.getAccessToken();
		
		assertNotNull(token1);
		assertNotNull(token2);
		assertEquals(token1, token2);
	}
	

	@Ignore
	@Test
	public void testGetAccessTokenWhenExpired() throws InterruptedException {
		SeekPostJobService service = new SeekPostJobService("Seek");
		
		IAccount account = new SeekAccount(SeekOAuth.TEST_CLIENT_ID, SeekOAuth.TEST_CLIENT_SECRET, SeekOAuth.TEST_ADVERTISER_ID);
		service.setAccount(account);
		
		String token1 = service.getAccessToken();
		
		account.setAccessTokenExpireIn(5);
		
		Thread.sleep(10 * 1000);
		
		String token2 = service.getAccessToken();
		
		assertNotNull(token1);
		assertNotNull(token2);
		assertNotEquals(token1, token2);
	}


	@Test
	public void testGetApiLinks(){
		SeekPostJobService service = new SeekPostJobService("Seek");
		
		
		IAccount account = new SeekAccount(SeekOAuth.TEST_CLIENT_ID, SeekOAuth.TEST_CLIENT_SECRET, SeekOAuth.TEST_ADVERTISER_ID);
		service.setAccount(account);
		
		Links links = service.getApiLinks();
		System.out.println(links.getAdvertisement().getHref());
		System.out.println(links.getAdvertisements().getHref());
		assertNotNull(links.getAdvertisement().getHref());
		assertNotNull(links.getAdvertisements().getHref());
	}
	
	@Ignore
	@Test
	public void testGetProcessingStatus() throws BadRequestException{
		
		SeekPostJobService service = new SeekPostJobService("Seek");
		
		
		IAccount account = new SeekAccount(SeekOAuth.TEST_CLIENT_ID, SeekOAuth.TEST_CLIENT_SECRET,  SeekOAuth.TEST_ADVERTISER_ID);
		service.setAccount(account);
		
		String link = service.getAdvertisementLink("add16acd-8bae-43f5-a0ae-5e6ede0760d9");
		assertNotNull(link);
		String status = service.getProcessingStatus(link);
		System.out.println(status);
		assertEquals("Completed", status);
	}
	
	@Ignore
	@Test
	public void testCreateJob() throws Exception{
		
		JobBoardPluginInterface driver = new SeekJobBoardPlugin("Seek");
		
		String jobJsonContent = JobBoardHelper.readFileToString("/SeekNewJob.json");
		JobBoardCredential credential = new JobBoardCredential( "00D90000000gtFz", "", SeekOAuth.TEST_ADVERTISER_ID, SeekOAuth.TEST_CLIENT_ID, SeekOAuth.TEST_CLIENT_SECRET );
		driver.createJob("00D90000000gtFz:a009000000E24Wb", "",jobJsonContent, credential );
		
	}
	
	
	@Test
	public void testUpdateJob() throws IOException{
		SeekPostJobService service = new SeekPostJobService("Seek");
		String jobJsonContent = JobBoardHelper.readFileToString("/SeekNewJob.json");
		service.updateJob( "00D90000000gtFz", "00D90000000gtFz:a009000000E24Wb","bb29702f-ade8-444f-9539-6ee6c7f5a6cd", JobBoard.SF_POSTING_STATUS_SUCCESS,  jobJsonContent,SeekOAuth.TEST_ADVERTISER_ID, SeekOAuth.TEST_CLIENT_ID, SeekOAuth.TEST_CLIENT_SECRET, "");
		
	}
	
	@Test
	public void testArchiveJob() throws IOException{
		SeekPostJobService service = new SeekPostJobService("Seek");
		
		service.archiveJob( "00D90000000gtFz", "00D90000000gtFz:a009000000E24Wb","bb29702f-ade8-444f-9539-6ee6c7f5a6cd", JobBoard.SF_POSTING_STATUS_SUCCESS,SeekOAuth.TEST_ADVERTISER_ID, SeekOAuth.TEST_CLIENT_ID, SeekOAuth.TEST_CLIENT_SECRET, "");
		
	}


}
