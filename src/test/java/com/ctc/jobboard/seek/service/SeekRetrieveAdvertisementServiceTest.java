package com.ctc.jobboard.seek.service;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.seek.domain.alljobs.Advertisement;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;

public class SeekRetrieveAdvertisementServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Ignore
	@Test
	public void testGetAllJobs() {
		SeekRetrieveAdvertisementService service = new SeekRetrieveAdvertisementService("Seek");
		
		List<Advertisement> ads = service.getAllJobs( "");
		System.out.println(ads.size());
//		for(Advertisement ad : ads){
//			System.out.println("-----------------------------");
//			System.out.println(ad.getId() );
//			System.out.println(ad.getJobTitle() );
//			System.out.println(ad.getLinks().getSelf().getHref());
//			
//			
//		}
	}

	@Test
	public void testGetJob() {
		SeekRetrieveAdvertisementService service = new SeekRetrieveAdvertisementService("Seek","Seek");
		
		SeekJobPostResponse response = service.getJob("8ff612a5-c4f4-4aa5-9a56-f34a660f8550");
		
		
		System.out.println(response.getId() );
		System.out.println(response.getJobTitle() );
		System.out.println(response.getLinks().getSelf().getHref());
		
	}


}
